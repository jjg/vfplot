/*
  private/spvol.h
  Copyright (c) J.J. Green 2020
*/

#ifndef PRIVATE_SPVOL_H
#define PRIVATE_SPVOL_H

#include <stddef.h>

int spvol(size_t, double*);

#endif
