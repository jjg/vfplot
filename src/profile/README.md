Profiling
---------

Some scripts for profiling with [gprof][1].  Run

```
make update-profile
```

on the unconfigured repository, this creates `prof/<version>.prof`.

The data here is only of interest to developers, so the directory
is excluded from the library distribution archives.

[1]: http://sourceware.org/binutils/docs/gprof/
