/*
  stats.h
  Copyright (c) J.J. Green 2022
*/

#ifndef STATS_H
#define STATS_H

#include <stdio.h>

typedef enum { cpu_rusage, cpu_wall, cpu_error } stats_cpu_type_t;
typedef enum { elapsed_present, elapsed_absent } stats_elapsed_type_t;
typedef enum { size_present, size_absent } stats_size_type_t;

typedef struct
{
  const char *version;
  stats_cpu_type_t cpu_type;
  union {
    struct {
      double user, sys;
    } rusage;
    double wall;
  } cpu;
  stats_elapsed_type_t elapsed_type;
  double elapsed;
  stats_size_type_t size_type;
  long size;
} stats_t;

int stats_write(FILE*, const stats_t*);

#endif
