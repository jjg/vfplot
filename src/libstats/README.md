libstats
--------

This is a small library defining a struct of summary statistics
on a plot and some functions to write that to JSON.  It is used
by `vfplot.c`, i.e., the top-level wrapper for the program and
not from other libraries.
