# -*- octave -*-
# No v martix, should fail

xrange = [0 2];
yrange = [0 1];

n = 128;

u = repmat([1:n]./n, n, 1);

save -v7 shear-v7-uv-absent.mat xrange yrange u;
