# -*- octave -*-
# should be OK

n = 128;

u = repmat([1:n]./n, n, 1);
v = zeros(n);

save -v7 shear-v7-range-absent.mat u v;
