#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "csv-grd.h"

#include <plot/csv-read.h>
#include <log.h>

#include <netcdf.h>
#include <unistd.h>

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct
{
  double min, max;
} range_t;

typedef struct
{
  range_t x, y;
} ranges_t;

typedef struct
{
  size_t x, y;
} sizes_t;

/*
  There a certain amount of duplication here with field_read()
  in libvf, but that creates a bilinear struct which is not quite
  what we need, so instead we share only the lower-level csv_read()
  in libplot.
*/

static int parse_grid(const char *arg, sizes_t *sizes)
{
  if (sscanf(arg, "%zi/%zi", &(sizes->x), &(sizes->y)) != 2)
    {
      log_error("failed parse of %s", arg);
      return 1;
    }

  return 0;
}

static int parse_ranges(const char *arg, ranges_t *ranges)
{
  if (sscanf(arg, "%lf/%lf/%lf/%lf",
             &(ranges->x.min),
             &(ranges->x.max),
             &(ranges->y.min),
             &(ranges->y.max)) != 4)
    {
      log_error("failed parse of %s", arg);
      return 1;
    }

  return 0;
}

static int scan_grid(const char *path, sizes_t *sizes)
{
  csv_read_t csv = {0};

  if (csv_read_open(path, &csv) != CSV_READ_OK)
    {
      log_error("failed CSV read initialise");
      return 1;
    }

  int err;
  size_t line = 0, record = 0;
  bool reading = true;
  size_t nmax[2] = {0, 0};

  do
    {
      size_t n[2];
      double z[2];

      err = csv_read_line(&csv, n, z);

      switch (err)
        {
        case CSV_READ_OK:
          line++;
          record++;
          for (size_t i = 0 ; i < 2 ; i++)
            if (nmax[i] < n[i]) nmax[i] = n[i];
          break;
        case CSV_READ_NODATA:
          line++;
          break;
        case CSV_READ_EOF:
          reading = false;
          break;
        default:
          log_error("CSV read error at line %zi", line + 1);
          reading = false;
        }
    }
  while (reading);

  csv_read_close(&csv);

  if ((err == CSV_READ_EOF) && (record > 0))
    {
      sizes->x = nmax[0] + 1;
      sizes->y = nmax[1] + 1;
      return 0;
    }
  else
    return 1;
}

static int grd_header(const sizes_t *sizes,
                      const ranges_t *ranges,
                      alignment_t align,
                      int fids[static 2])
{
  for (size_t i = 0 ; i < 2 ; i++)
    {
      int fid = fids[i];
      int nc_err;

      /* dimensions */

      int didx, didy;

      nc_err = nc_def_dim(fid, "x", sizes->x, &didx);

      if (nc_err != NC_NOERR)
        {
          log_error("nc_def_dim(x): %s", nc_strerror(nc_err));
          return 1;
        }

      nc_err = nc_def_dim(fid, "y", sizes->y, &didy);

      if (nc_err != NC_NOERR)
        {
          log_error("nc_def_dim(y): %s", nc_strerror(nc_err));
          return 1;
        }

      /* variables */

      int vidx, vidy;

      nc_err = nc_def_var(fid, "x", NC_DOUBLE, 1, &didx, &vidx);

      if (nc_err != NC_NOERR)
        {
          log_error("nc_def_var(x): %s", nc_strerror(nc_err));
          return 1;
        }

      nc_err = nc_def_var(fid, "y", NC_DOUBLE, 1, &didy, &vidy);

      if (nc_err != NC_NOERR)
        {
          log_error("nc_def_var(y): %s", nc_strerror(nc_err));
          return 1;
        }

      int vidz, didz[2] = {didy, didx};

      nc_err = nc_def_var(fid, "z", NC_FLOAT, 2, didz, &vidz);

      if (nc_err != NC_NOERR)
        {
          log_error("nc_def_var(z): %s", nc_strerror(nc_err));
          return 1;
        }

      /* actual range attribute */

      double arx[2] = { ranges->x.min, ranges->x.max };
      nc_err = nc_put_att_double(fid, vidx, "actual_range",
                                 NC_DOUBLE, 2, arx);

      if (nc_err != NC_NOERR)
        {
          log_error("nc_put_attr(x:actual_range): %s", nc_strerror(nc_err));
          return 1;
        }

      double ary[2] = { ranges->y.min, ranges->y.max };
      nc_err = nc_put_att_double(fid, vidy, "actual_range",
                                 NC_DOUBLE, 2, ary);

      if (nc_err != NC_NOERR)
        {
          log_error("nc_put_attr(y:actual_range): %s", nc_strerror(nc_err));
          return 1;
        }

      /* variable long-name attributes */

      nc_err = nc_put_att_text(fid, vidx, "long_name", 1, "x");

      if (nc_err != NC_NOERR)
        {
          log_error("nc_put_attr(x, long_name): %s", nc_strerror(nc_err));
          return 1;
        }

      nc_err = nc_put_att_text(fid, vidy, "long_name", 1, "y");

      if (nc_err != NC_NOERR)
        {
          log_error("nc_put_attr(y, long_name): %s", nc_strerror(nc_err));
          return 1;
        }

      nc_err = nc_put_att_text(fid, vidz, "long_name", 1, "z");

      if (nc_err != NC_NOERR)
        {
          log_error("nc_put_attr(z, long_name): %s", nc_strerror(nc_err));
          return 1;
        }

      /* z-variable fill value */

      float fill_val[] = {NAN};
      nc_err = nc_put_att_float(fid, vidz, "_FillValue", NC_FLOAT, 1, fill_val);

      if (nc_err != NC_NOERR)
        {
          log_error("nc_put_attr(z, _FillValue): %s", nc_strerror(nc_err));
          return 1;
        }

      /* global attributes */

      const char conventions[] = "COARDS/CF-1.0";
      nc_err = nc_put_att_text(fid, NC_GLOBAL,
                               "Conventions",
                               strlen(conventions),
                               conventions);

      if (nc_err != NC_NOERR)
        {
          log_error("nc_put_attr(Conventions): %s", nc_strerror(nc_err));
          return 1;
        }

      const char title[] = "vfplot/csv-grd output";
      nc_err = nc_put_att_text(fid, NC_GLOBAL,
                               "title",
                               strlen(title),
                               title);

      if (nc_err != NC_NOERR)
        {
          log_error("nc_put_attr(title): %s", nc_strerror(nc_err));
          return 1;
        }

      int node_offset;

      if (align == node_aligned)
        node_offset = 0;
      else
        node_offset = 1;

      nc_err = nc_put_att_int(fid, NC_GLOBAL, "node_offset",
                              NC_INT, 1, &node_offset);

      if (nc_err != NC_NOERR)
        {
          log_error("nc_put_attr(node_offset): %s", nc_strerror(nc_err));
          return 1;
        }

      nc_err = nc_enddef(fid);

      if (nc_err != NC_NOERR)
        {
          log_error("nc_enddef: %s", nc_strerror(nc_err));
          return 1;
        }
    }

  return 0;
}

/* write x and y data to one file files */

static int grd_1d_file(int fid, const char *name, double *val)
{
  int vid;

  if ((vid = ncvarid(fid, name)) == -1)
    {
      log_error("failed to get id for variable %s", name);
      return 1;
    }

  int nc_err = nc_put_var(fid, vid, val);

  if (nc_err != NC_NOERR)
    {
      log_error("nc_put_var(%s): %s", name, nc_strerror(nc_err));
      return 1;
    }

  return 0;
}

/* write x or y data to both files */

static int grd_1d(size_t n,
                  const range_t *range,
                  alignment_t align,
                  const char *name,
                  int fids[static 2])
{
  double *val;

  if ((val = calloc(n, sizeof(double))) == NULL)
    log_error("calloc %zi doubles", n);
  else
    {
      double val0, val1;

      if (align == node_aligned)
        {
          val0 = range->min;
          val1 = range->max;
        }
      else
        {
          double pw = (range->max - range->min) / n;

          val0 = range->min + pw / 2;
          val1 = range->max - pw / 2;
        }

      for (size_t i = 0 ; i < n ; i++)
        val[i] = (i * val1 + (n - i - 1) * val0) / (n - 1);

      int err = 0;

      for (size_t i = 0 ; i < 2 ; i++)
        err += grd_1d_file(fids[i], name, val);

      free(val);

      if (err == 0) return 0;
    }

  return 1;
}

/* write x and y data to both files */

static int grd_xy(const sizes_t *sizes,
                  const ranges_t *ranges,
                  alignment_t align,
                  int fids[static 2])
{
  if (grd_1d(sizes->x, &(ranges->x), align, "x", fids) != 0)
    return 1;

  if (grd_1d(sizes->y, &(ranges->y), align, "y", fids) != 0)
    return 1;

  return 0;
}

/* write z data to files */

static int grd_z(const char *path,
                 const sizes_t *sizes,
                 int fids[static 2])
{
  csv_read_t csv = {0};

  if (csv_read_open(path, &csv) != CSV_READ_OK)
    {
      log_error("failed open of %s", path);
      return 1;
    }

  int err = 0;
  size_t nx = sizes->x, ny = sizes->y;

  /* Z is dynamically allocared Z[2][ny][nx] */

  float (*Z)[ny][nx];

  if ((Z = malloc(2 * sizeof(*Z))) != NULL)
    {
      size_t line = 0;
      bool reading = true;

      for (size_t i = 0 ; i < nx ; i++)
        for (size_t j = 0 ; j < ny ; j++)
          for (size_t k = 0 ; k < 2 ; k++)
            Z[k][j][i] = NAN;

      do
        {
          size_t n[2];
          double z[2];

          int csv_err = csv_read_line(&csv, n, z);

          switch (csv_err)
            {
            case CSV_READ_OK:
              line++;
              if ((n[0] < nx) && (n[1] < ny))
                {
                  Z[0][n[1]][n[0]] = z[0];
                  Z[1][n[1]][n[0]] = z[1];
                }
              break;
            case CSV_READ_NODATA:
              line++;
              break;
            case CSV_READ_EOF:
              reading = false;
              break;
            default:
              err++;
              log_error("CSV read error at line %zi", line + 1);
              reading = false;
            }
        }
      while (reading);

      for (size_t i = 0 ; (i < 2) && (!err) ; i++)
        {
          int fid = fids[i], vid;

          if ((vid = ncvarid(fid, "z")) == -1)
            {
              log_error("failed to get id for variable z");
              err++;
            }

          int nc_err = nc_put_var(fid, vid, Z[i]);

          if (nc_err != NC_NOERR)
            {
              log_error("nc_put_var(z): %s", nc_strerror(nc_err));
              err++;
            }
        }

      free(Z);
    }

  csv_read_close(&csv);

  if (err == 0)
    return 0;
  else
    return 1;
}

static int csv_grd2(const char *path,
                    const sizes_t *sizes,
                    const ranges_t *ranges,
                    alignment_t align,
                    int fids[static 2])
{
  if (grd_header(sizes, ranges, align, fids) != 0)
    {
      log_error("failed to write netCDF header");
      return 1;
    }

  if (grd_xy(sizes, ranges, align, fids) != 0)
    {
      log_error("failed to write netCDF x-data");
      return 1;
    }

  if (grd_z(path, sizes, fids) != 0)
    {
      log_error("failed to write netCDF x-data");
      return 1;
    }

  return 0;
}

int csv_grd(csv_grd_t *opt)
{
  sizes_t sizes;

  if (opt->grid.size.arg == NULL)
    {
      if (scan_grid(opt->file.in, &sizes) != 0)
        {
          log_error("failed scan of grid-size");
          return 1;
        }
    }
  else
    {
      if (parse_grid(opt->grid.size.arg, &sizes) != 0)
        {
          log_error("failed parse of grid-size");
          return 1;
        }
    }

  if ((sizes.x <= 1) || (sizes.y <= 1))
    {
      log_error("infeasible grid-size: %zi x %zi grid",
                sizes.x, sizes.y);
      return 1;
    }

  log_info("grid-size: %zi x %zi", sizes.x, sizes.y);

  alignment_t align = opt->grid.align;

  if (align == node_aligned)
    log_info("node (gridline) aligned");
  else
    log_info("pixel aligned");

  ranges_t ranges;

  if (opt->grid.range.arg == NULL)
    {
      ranges.x.min = 0;
      ranges.x.max = sizes.x - 1;
      ranges.y.min = 0;
      ranges.y.max = sizes.y - 1;
    }
  else
    {
      if (parse_ranges(opt->grid.range.arg, &ranges) != 0)
        {
          log_error("failed parse of grid-range");
          return 1;
        }
    }

  log_info("grid extent: [%g, %g] x [%g, %g]",
           ranges.x.min,
           ranges.x.max,
           ranges.y.min,
           ranges.y.max);

  if ((ranges.x.min >= ranges.x.max) || (ranges.y.min >= ranges.y.max))
    {
      log_error("infeasible grid-range: [%lg, %lg] x [%lg, %lg]",
                ranges.x.min, ranges.x.max,
                ranges.y.min, ranges.y.max);
      return 1;
    }

  int fids[2];

  for (size_t i = 0 ; i < 2 ; i++)
    {
      const char *path = opt->file.out[i];
      int nc_err;

      if ((nc_err = nc_create(path, NC_CLOBBER, fids + i)) != NC_NOERR)
        {
          log_error("nc_create: %s", nc_strerror(nc_err));
          return 1;
        }
    }

  int err = csv_grd2(opt->file.in, &sizes, &ranges, align, fids);

  for (size_t i = 0 ; i < 2 ; i++)
    {
      int nc_err;

      if ((nc_err = nc_close(fids[i])) != NC_NOERR)
        {
          log_error("nc_close: %s", nc_strerror(nc_err));
          err++;
        }
    }

  if (err != 0)
    {
      for (size_t i = 0 ; i < 2 ; i++)
        unlink(opt->file.out[i]);
      return 1;
    }

  return 0;
}
