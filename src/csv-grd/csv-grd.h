/*
  csv-grd.h

  read vfplot CSV files and write 2 GMT netCDF (grd) files

  J.J. Green 2023
*/

#ifndef CSV_GRD_H
#define CSV_GRD_H

#include <stdbool.h>
#include <stddef.h>

typedef enum { node_aligned, pixel_aligned } alignment_t;

typedef struct
{
  bool verbose;
  struct {
    struct {
      const char *arg;
    } range;
    struct {
      const char *arg;
    } size;
    alignment_t align;
  } grid;
  struct {
    const char *in;
    const char *out[2];
  } file;
} csv_grd_t;

int csv_grd(csv_grd_t*);

#endif
