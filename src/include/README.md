Build headers
-------------

This directory is the installation target for the libraries which
are part of the build of **vfplot**, we do this since there are
several such libraries, it was becoming combersome to have multiple
`-I` flags passed to the C compiler, with this "internal install" as
part of the build, we only need the one: `-I../include`; similarly
with the sibling `lib` directory for the libraries themselves.

There are `.gitignore` files to ignore these temporarily installed
headers, but also to ensure the subdirectories exist on git checkout
(it is tiresome to create/destroy them as part of the build).
