#ifndef FIELD_CSV_H
#define FIELD_CSV_H

#include "vf/field.h"

field_t* field_read_csv(const char*,
                        const field_grid_t*,
                        const field_range_t*,
                        field_align_t);

#endif
