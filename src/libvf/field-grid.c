#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "vf/field-grid.h"

bool field_grid_valid(const field_grid_t *grid)
{
  return (grid->size[0] > 0) && (grid->size[1] > 0);
}
