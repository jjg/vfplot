#ifndef VF_FIELD_GRID_H
#define VF_FIELD_GRID_H

#include <stdbool.h>
#include <stddef.h>

typedef struct {
  size_t size[2];
} field_grid_t;

bool field_grid_valid(const field_grid_t*);

#endif
