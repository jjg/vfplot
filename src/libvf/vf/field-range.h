#ifndef VF_FIELD_RANGE_H
#define VF_FIELD_RANGE_H

#include <stdbool.h>

typedef struct {
  struct { double min, max; } x, y;
} field_range_t;

bool field_range_valid(const field_range_t*);

#endif
