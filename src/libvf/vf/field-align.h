#ifndef VF_FIELD_ALIGN_H
#define VF_FIELD_ALIGN_H

typedef enum {
  align_none,
  align_pixel,
  align_node
} field_align_t;

#endif
