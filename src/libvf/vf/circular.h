#ifndef VF_CIRCULAR_H
#define VF_CIRCULAR_H

#include <vf/type.h>

typedef struct
{
  double scale, width, height;
} circular_field_t;

extern vf_methods_t circular_methods;

#endif
