#ifndef VF_TYPE_H
#define VF_TYPE_H

#include <plot/domain.h>

typedef struct
{
  int (*vector)(void*, double, double, double*, double*);
  int (*curvature)(void*, double, double, double*);
  domain_t* (*domain)(const void*);
  void (*scale)(void*, double);
} vf_methods_t;

#endif
