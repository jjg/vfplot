#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "field-range-bbox.h"

/*
  We need to the the bounding box of the node-aligned grid in
  the bilinear struct.  For node-aligned, this is just the range
  (if specified), but for pixel aligned we need to shave a half
  pixel of each side of the specified range -- this is done in
  both field-mat.c and field-csv.c, so extracted here.
*/

int field_range_bbox(const field_range_t *range,
                     field_align_t align,
                     size_t nx, size_t ny,
                     bbox_t *bbox)
{
  switch (align)
    {
    case align_node:

      if (range == NULL)
        {
          bbox->x.min = 0;
          bbox->x.max = nx - 1;
          bbox->y.min = 0;
          bbox->y.max = ny - 1;
        }
      else
        {
          bbox->x.min = range->x.min;
          bbox->x.max = range->x.max;
          bbox->y.min = range->y.min;
          bbox->y.max = range->y.max;
        }
      break;

    case align_pixel:

      if (range == NULL)
        {
          bbox->x.min = 0.5;
          bbox->x.max = nx - 0.5;
          bbox->y.min = 0.5;
          bbox->y.max = ny - 0.5;
        }
      else
        {
          double
            dx = 0.5 * (range->x.max - range->x.min) / nx,
            dy = 0.5 * (range->y.max - range->y.min) / ny;

          bbox->x.min = range->x.min + dx;
          bbox->x.max = range->x.max - dx;
          bbox->y.min = range->y.min + dy;
          bbox->y.max = range->y.max - dy;
        }
      break;

    default:
      return 1;
    }

  return 0;
}
