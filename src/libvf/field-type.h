#ifndef FIELD_TYPE_H
#define FIELD_TYPE_H

#include "vf/field-align.h"

#include <plot/bilinear.h>

struct field_t
{
  field_align_t align;
  double scale;
  bilinear_t *v, *k;
};

#endif
