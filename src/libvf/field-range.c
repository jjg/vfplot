#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "vf/field-range.h"

bool field_range_valid(const field_range_t *range)
{
  return
    (range->x.min < range->x.max) &&
    (range->y.min < range->y.max);
}
