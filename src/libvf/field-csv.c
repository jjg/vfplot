#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>

#include <plot/csv-read.h>

#include <log.h>

#include "vf/field.h"
#include "field-csv.h"
#include "field-type.h"
#include "field-range-bbox.h"

static int field_read_size(const char *file, field_grid_t *grid)
{
  csv_read_t C;

  if (csv_read_open(file, &C) !=  CSV_READ_OK)
    {
      log_error("failed open of %s", file);
      return 1;
    }

  size_t read = 0;
  int err;
  size_t nmax[2] = {0};

  do
    {
      size_t n[2];
      double z[2];

      err = csv_read_line(&C, n, z);

      switch (err)
        {
        case CSV_READ_OK:
          for (size_t i = 0 ; i < 2 ; i++)
            if (nmax[i] < n[i]) nmax[i] = n[i];
	  /* fallthrough */
	case CSV_READ_NODATA:
	  read++;
	  /* fallthrough */
	case CSV_READ_EOF:
	  break;
	default:
	  log_error("CSV read error at line %zi", read + 1);
          goto read_size_cleanup;
        }
    }
  while (err != CSV_READ_EOF);

  grid->size[0] = nmax[0] + 1;
  grid->size[1] = nmax[1] + 1;

 read_size_cleanup:

  csv_read_close(&C);

  return err == CSV_READ_EOF ? 0 : 1;
}

field_t* field_read_csv(const char *file,
                        const field_grid_t *grid,
                        const field_range_t *range,
                        field_align_t align)
{
  if (grid == NULL)
    {
      field_grid_t grid2;
      if (field_read_size(file, &grid2) != 0)
        {
          log_error("failed to read index columns in %s", file);
          return NULL;
        }

      return field_read_csv(file, &grid2, range, align);
    }

  /*
    We need to the the bounding box of the node-aligned grid in
    the bilinear struct.  For node-aligned, this is just the range
    (if specified), but for pixel aligned we need to shave a half
    pixel of each side of the specified range
  */

  size_t
    nx = grid->size[0],
    ny = grid->size[1];
  bbox_t bbox;

  if (field_range_bbox(range, align, nx, ny, &bbox) != 0)
    {
      log_error("failed to get bounding-box of range");
      return NULL;
    }

  csv_read_t
    C = { .grid = { .n = { nx, ny } }};

  if (csv_read_open(file, &C) !=  CSV_READ_OK)
    {
      log_error("failed open of %s", file);
      return NULL;
    }

  bilinear_t *B;

  if ((B = bilinear_new(nx, ny, 2)) == NULL)
    return NULL;

  bilinear_bbox_set(B, bbox);

  size_t read = 0, set = 0;
  int err;

  do
    {
      size_t n[2];
      double z[2];

      err = csv_read_line(&C, n, z);

      switch (err)
        {
        case CSV_READ_OK:
	  set++;
          bilinear_z_set(B, n[0], n[1], z);
	  /* fallthrough */
	case CSV_READ_NODATA:
	  read++;
	  /* fallthrough */
	case CSV_READ_EOF:
	  break;
	default:
	  log_error("error at line %zi", read + 1);
          goto read_csv_cleanup;
        }
    }
  while (err != CSV_READ_EOF);

  csv_read_close(&C);

  if (read == 0)
    {
      log_error("csv file has no data lines");
      goto read_csv_cleanup;
    }

  if (set == 0)
    {
      log_error("read %zi nodata lines - bad csv header?", read);
      goto read_csv_cleanup;
    }

  field_t *F;

  if ((F = malloc(sizeof(field_t))) == NULL)
    goto read_csv_cleanup;

  F->align = align;
  F->v = B;
  F->k = NULL;

  return F;

 read_csv_cleanup:

  bilinear_destroy(B);

  return NULL;
}
