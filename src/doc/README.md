Docbook code
------------

The `root` directory contains sources for the bottom-level
documentation (`INSTALL.md` etc), this is also used to
generate pages on the project's website.

The `man` directory contains the manual pages for the programs
and file-formats of the package.
