#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/scan-colour.h"
#include "plot/svg-colours.h"
#include "plot/error.h"

#include <log.h>

#include <stddef.h>
#include <stdio.h>

int scan_colour(const char *arg, colour_t *pC)
{
  colour_t C = { .type = colour_none };

  const struct svg_colour_t *named = svg_colour(arg);

  if (named != NULL)
    {
      C.type = colour_rgb;
      C.rgb.r = named->r;
      C.rgb.g = named->g;
      C.rgb.b = named->b;
    }
  else
    {
      unsigned char k[3];

      switch (sscanf(arg, "%hhu/%hhu/%hhu", k + 0, k + 1, k + 2))
        {
        case 1:
          C.type = colour_grey;
          C.grey = k[0];
          break;

        case 3:
          C.type = colour_rgb;
          C.rgb.r = k[0];
          C.rgb.g = k[1];
          C.rgb.b = k[2];
          break;

        default :
          log_error("malformed colour: %s", arg);
          return ERROR_USER;
        }
    }

  *pC = C;

  return ERROR_OK;
}
