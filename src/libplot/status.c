#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>

#include <log.h>

#include "plot/status.h"


static int nlen = 4, slen = 12;

void status_set_length(int nl, int sl)
{
  nlen = nl;
  slen = sl;
}

/*
  produce status messages indented and justified, like
  - initial    120
  - decmated    34

  The %-*s is a %s (string) with right justification (-)
  the the width in the function argument (*)
*/

void status(const char *s, int n)
{
  log_info("  %-*s%*i", slen, s, nlen, n);
}
