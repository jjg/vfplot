#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>

#include <log.h>

#include "plot/csv-read.h"

#define MAX_DATA_LINE 1024

static int csv_read_stream(FILE *st,  csv_read_t *S)
{
  S->st = st;
  return CSV_READ_OK;
}

int csv_read_open(const char *path, csv_read_t *S)
{
  FILE *st;

  if ((st = fopen(path, "r")) == NULL)
    {
      log_error("failed open of %s", path);
      return CSV_READ_ERROR;
    }

  int err = csv_read_stream(st, S);

  if (err != CSV_READ_OK)
    fclose(st);

  return err;
}

/*
  this function may return

  OK     - the values were read and set
  NODATA - the read value was not on the grid
  EOF    - end of file
  ERROR  - duh
*/

int csv_read_line(const csv_read_t *S,
                  size_t n[static 2],
                  double v[static 2])
{
  char line[MAX_DATA_LINE];
  FILE *st = S->st;

  if (ferror(st))
    {
      log_error("error on stream");
      return CSV_READ_ERROR;
    }

  /* read line */

  if (fgets(line, MAX_DATA_LINE, st) == NULL)
    {
      if (feof(st))
        return CSV_READ_EOF;
      log_error("failed line read");
      return CSV_READ_ERROR;
    }

  int err = sscanf(line,
                   "%zi,%zi,%lf,%lf",
                   &(n[0]), &(n[1]),
                   &(v[0]), &(v[1]));

  if (err == 4)
    return CSV_READ_OK;
  else
    return CSV_READ_ERROR;
}

void csv_read_close(csv_read_t *S)
{
  fclose(S->st);
}
