#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/scan-pen.h"
#include "plot/scan-colour.h"
#include "plot/scan-length.h"
#include "plot/error.h"

#include <log.h>

#include <stddef.h>
#include <string.h>

const pen_t scan_pen_default = {
  .width = 0,
  .colour = {
    .type = colour_none
  }
};

int scan_pen(const char *str, pen_t *pen)
{
  size_t n;

  if ((n = strlen(str)) == 0)
    {
      log_error("empty pen");
      return ERROR_USER;
    }

  char buffer[n + 1];

  memcpy(buffer, str, n + 1);

  int err;
  double width;
  colour_t colour;
  char *p;

  if ((p = strchr(buffer, '/')))
    {
      *p = '\0'; p++;

      if ((err = scan_colour(p, &colour)) != ERROR_OK)
        {
          log_error("error scanning pen-colour");
          return err;
        }
    }
  else
    {
      colour.type = colour_grey;
      colour.grey = 0;
    }

  if ((err = scan_length(buffer, "pen-width", &width)) != ERROR_OK)
    {
      log_error("error scanning pen-width");
      return err;
    }

  if (width < 0.0)
    {
      log_error("pen width is negative (%g)", width);
      return ERROR_USER;
    }

  pen->width = width;
  pen->colour = colour;

  return ERROR_OK;
}
