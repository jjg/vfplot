/* ANSI-C code produced by gperf version 3.1 */
/* Command-line: /usr/bin/gperf --delimiters , --struct-type --language ANSI-C --lookup-function-name svg_colours_lookup --ignore-case --readonly-tables --enum --output-file svg-colours.c svg-colours.gperf  */
/* Computed positions: -k'1,3,6-8,12-13' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
#error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gperf@gnu.org>."
#endif

#line 1 "svg-colours.gperf"

  /*
    The list of names and RGB triples is extracted from the
    SVG 1.1 (Second Edition) – 16 August 2011, section 4
  */
#include <stdlib.h>
#include <string.h>
#include "plot/svg-colours.h"
#line 11 "svg-colours.gperf"
struct svg_colour_t;
/* maximum key range = 562, duplicates = 0 */

#ifndef GPERF_DOWNCASE
#define GPERF_DOWNCASE 1
static unsigned char gperf_downcase[256] =
  {
      0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,
     15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,
     30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,
     45,  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,
     60,  61,  62,  63,  64,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106,
    107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121,
    122,  91,  92,  93,  94,  95,  96,  97,  98,  99, 100, 101, 102, 103, 104,
    105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
    120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134,
    135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149,
    150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164,
    165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179,
    180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194,
    195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209,
    210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224,
    225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
    240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254,
    255
  };
#endif

#ifndef GPERF_CASE_STRCMP
#define GPERF_CASE_STRCMP 1
static int
gperf_case_strcmp (register const char *s1, register const char *s2)
{
  for (;;)
    {
      unsigned char c1 = gperf_downcase[(unsigned char)*s1++];
      unsigned char c2 = gperf_downcase[(unsigned char)*s2++];
      if (c1 != 0 && c1 == c2)
        continue;
      return (int)c1 - (int)c2;
    }
}
#endif

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
hash (register const char *str, register size_t len)
{
  static const unsigned short asso_values[] =
    {
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566,   5,  55,   0,  35,   0,
       75,  10,   5,   0, 566, 250,  10,  40,  85,  60,
       70, 144,   0,  20,  45,  10,  30, 185,  95, 195,
      566,   0, 566, 566, 566, 566, 566,   5,  55,   0,
       35,   0,  75,  10,   5,   0, 566, 250,  10,  40,
       85,  60,  70, 144,   0,  20,  45,  10,  30, 185,
       95, 195, 566,   0, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566
    };
  register unsigned int hval = len;

  switch (hval)
    {
      default:
        hval += asso_values[(unsigned char)str[12]];
      /*FALLTHROUGH*/
      case 12:
        hval += asso_values[(unsigned char)str[11]];
      /*FALLTHROUGH*/
      case 11:
      case 10:
      case 9:
      case 8:
        hval += asso_values[(unsigned char)str[7]];
      /*FALLTHROUGH*/
      case 7:
        hval += asso_values[(unsigned char)str[6]];
      /*FALLTHROUGH*/
      case 6:
        hval += asso_values[(unsigned char)str[5]];
      /*FALLTHROUGH*/
      case 5:
      case 4:
      case 3:
        hval += asso_values[(unsigned char)str[2]+2];
      /*FALLTHROUGH*/
      case 2:
      case 1:
        hval += asso_values[(unsigned char)str[0]];
        break;
    }
  return hval;
}

const struct svg_colour_t *
svg_colours_lookup (register const char *str, register size_t len)
{
  enum
    {
      TOTAL_KEYWORDS = 147,
      MIN_WORD_LENGTH = 3,
      MAX_WORD_LENGTH = 20,
      MIN_HASH_VALUE = 4,
      MAX_HASH_VALUE = 565
    };

  static const struct svg_colour_t wordlist[] =
    {
      {""}, {""}, {""}, {""},
#line 33 "svg-colours.gperf"
      {"cyan", 0, 255, 255},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 66 "svg-colours.gperf"
      {"gray", 128, 128, 128},
      {""}, {""}, {""}, {""}, {""},
#line 27 "svg-colours.gperf"
      {"chartreuse", 127, 255, 0},
      {""}, {""}, {""},
#line 67 "svg-colours.gperf"
      {"grey", 128, 128, 128},
#line 68 "svg-colours.gperf"
      {"green", 0, 128, 0},
      {""}, {""}, {""},
#line 86 "svg-colours.gperf"
      {"lightgrey", 211, 211, 211},
#line 85 "svg-colours.gperf"
      {"lightgreen", 144, 238, 144},
      {""}, {""}, {""},
#line 84 "svg-colours.gperf"
      {"lightgray", 211, 211, 211},
      {""}, {""},
#line 142 "svg-colours.gperf"
      {"skyblue", 135, 206, 235},
      {""},
#line 145 "svg-colours.gperf"
      {"slategrey", 112, 128, 144},
      {""},
#line 140 "svg-colours.gperf"
      {"sienna", 160, 82, 45},
      {""}, {""},
#line 144 "svg-colours.gperf"
      {"slategray", 112, 128, 144},
      {""}, {""}, {""},
#line 139 "svg-colours.gperf"
      {"seashell", 255, 245, 238},
#line 150 "svg-colours.gperf"
      {"teal", 0, 128, 128},
#line 29 "svg-colours.gperf"
      {"coral", 255, 127, 80},
      {""}, {""}, {""}, {""}, {""},
#line 88 "svg-colours.gperf"
      {"lightsalmon", 255, 160, 122},
      {""}, {""},
#line 92 "svg-colours.gperf"
      {"lightslategrey", 119, 136, 153},
#line 20 "svg-colours.gperf"
      {"black", 0, 0, 0},
      {""}, {""}, {""},
#line 91 "svg-colours.gperf"
      {"lightslategray", 119, 136, 153},
      {""},
#line 118 "svg-colours.gperf"
      {"orange", 255, 165, 0},
      {""}, {""},
#line 119 "svg-colours.gperf"
      {"orangered", 255, 69, 0},
      {""},
#line 19 "svg-colours.gperf"
      {"bisque", 255, 228, 196},
      {""}, {""},
#line 95 "svg-colours.gperf"
      {"lime", 0, 255, 0},
      {""}, {""}, {""},
#line 132 "svg-colours.gperf"
      {"red", 255, 0, 0},
#line 96 "svg-colours.gperf"
      {"limegreen", 50, 205, 50},
#line 81 "svg-colours.gperf"
      {"lightcoral", 240, 128, 128},
      {""}, {""}, {""},
#line 134 "svg-colours.gperf"
      {"royalblue", 65, 105, 225},
#line 97 "svg-colours.gperf"
      {"linen", 250, 240, 230},
      {""},
#line 61 "svg-colours.gperf"
      {"fuchsia", 255, 0, 255},
      {""},
#line 38 "svg-colours.gperf"
      {"darkgreen", 0, 100, 0},
      {""}, {""}, {""}, {""},
#line 80 "svg-colours.gperf"
      {"lightblue", 173, 216, 230},
#line 44 "svg-colours.gperf"
      {"darkorchid", 153, 50, 204},
#line 147 "svg-colours.gperf"
      {"springgreen", 0, 255, 127},
#line 98 "svg-colours.gperf"
      {"magenta", 255, 0, 255},
      {""},
#line 64 "svg-colours.gperf"
      {"gold", 255, 215, 0},
      {""},
#line 120 "svg-colours.gperf"
      {"orchid", 218, 112, 214},
      {""}, {""},
#line 143 "svg-colours.gperf"
      {"slateblue", 106, 90, 205},
      {""},
#line 41 "svg-colours.gperf"
      {"darkmagenta", 139, 0, 139},
      {""},
#line 34 "svg-colours.gperf"
      {"darkblue", 0, 0, 139},
#line 93 "svg-colours.gperf"
      {"lightsteelblue", 176, 196, 222},
      {""},
#line 141 "svg-colours.gperf"
      {"silver", 192, 192, 192},
      {""},
#line 138 "svg-colours.gperf"
      {"seagreen", 46, 139, 87},
#line 148 "svg-colours.gperf"
      {"steelblue", 70, 130, 180},
      {""}, {""}, {""},
#line 149 "svg-colours.gperf"
      {"tan", 210, 180, 140},
#line 127 "svg-colours.gperf"
      {"peru", 205, 133, 63},
      {""},
#line 131 "svg-colours.gperf"
      {"purple", 128, 0, 128},
#line 45 "svg-colours.gperf"
      {"darkred", 139, 0, 0},
      {""},
#line 110 "svg-colours.gperf"
      {"mintcream", 245, 255, 250},
      {""}, {""}, {""}, {""},
#line 58 "svg-colours.gperf"
      {"firebrick", 178, 34, 34},
      {""}, {""}, {""},
#line 89 "svg-colours.gperf"
      {"lightseagreen", 32, 178, 170},
#line 42 "svg-colours.gperf"
      {"darkolivegreen", 85, 107, 47},
      {""}, {""}, {""}, {""},
#line 111 "svg-colours.gperf"
      {"mistyrose", 255, 228, 225},
      {""},
#line 73 "svg-colours.gperf"
      {"indigo", 75, 0, 130},
#line 115 "svg-colours.gperf"
      {"oldlace", 253, 245, 230},
      {""},
#line 128 "svg-colours.gperf"
      {"pink", 255, 192, 203},
#line 46 "svg-colours.gperf"
      {"darksalmon", 233, 150, 122},
      {""}, {""},
#line 76 "svg-colours.gperf"
      {"lavender", 230, 230, 250},
#line 74 "svg-colours.gperf"
      {"ivory", 255, 255, 240},
      {""}, {""}, {""},
#line 112 "svg-colours.gperf"
      {"moccasin", 255, 228, 181},
      {""}, {""}, {""}, {""}, {""},
#line 26 "svg-colours.gperf"
      {"cadetblue", 95, 158, 160},
#line 52 "svg-colours.gperf"
      {"darkviolet", 148, 0, 211},
#line 135 "svg-colours.gperf"
      {"saddlebrown", 139, 69, 19},
      {""},
#line 48 "svg-colours.gperf"
      {"darkslateblue", 72, 61, 139},
#line 122 "svg-colours.gperf"
      {"palegreen", 152, 251, 152},
      {""}, {""}, {""},
#line 146 "svg-colours.gperf"
      {"snow", 255, 250, 250},
#line 72 "svg-colours.gperf"
      {"indianred", 205, 92, 92},
#line 83 "svg-colours.gperf"
      {"lightgoldenrodyellow", 250, 250, 210},
#line 152 "svg-colours.gperf"
      {"tomato", 255, 99, 71},
#line 79 "svg-colours.gperf"
      {"lemonchiffon", 255, 250, 205},
      {""},
#line 87 "svg-colours.gperf"
      {"lightpink", 255, 182, 193},
      {""},
#line 99 "svg-colours.gperf"
      {"maroon", 128, 0, 0},
      {""},
#line 77 "svg-colours.gperf"
      {"lavenderblush", 255, 240, 245},
#line 153 "svg-colours.gperf"
      {"turquoise", 64, 224, 208},
#line 43 "svg-colours.gperf"
      {"darkorange", 255, 140, 0},
      {""}, {""}, {""},
#line 114 "svg-colours.gperf"
      {"navy", 0, 0, 128},
#line 57 "svg-colours.gperf"
      {"dodgerblue", 30, 144, 255},
#line 60 "svg-colours.gperf"
      {"forestgreen", 34, 139, 34},
#line 109 "svg-colours.gperf"
      {"midnightblue", 25, 25, 112},
      {""},
#line 104 "svg-colours.gperf"
      {"mediumseagreen", 60, 179, 113},
      {""}, {""},
#line 47 "svg-colours.gperf"
      {"darkseagreen", 143, 188, 143},
      {""},
#line 15 "svg-colours.gperf"
      {"aqua", 0, 255, 255},
#line 17 "svg-colours.gperf"
      {"azure", 240, 255, 255},
#line 136 "svg-colours.gperf"
      {"salmon", 250, 128, 114},
      {""}, {""}, {""},
#line 155 "svg-colours.gperf"
      {"wheat", 245, 222, 179},
      {""}, {""}, {""},
#line 24 "svg-colours.gperf"
      {"brown", 165, 42, 42},
#line 16 "svg-colours.gperf"
      {"aquamarine", 127, 255, 212},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 28 "svg-colours.gperf"
      {"chocolate", 210, 105, 30},
#line 78 "svg-colours.gperf"
      {"lawngreen", 124, 252, 0},
#line 137 "svg-colours.gperf"
      {"sandybrown", 244, 164, 96},
      {""}, {""}, {""},
#line 82 "svg-colours.gperf"
      {"lightcyan", 224, 255, 255},
      {""}, {""}, {""}, {""}, {""},
#line 154 "svg-colours.gperf"
      {"violet", 238, 130, 238},
#line 94 "svg-colours.gperf"
      {"lightyellow", 255, 255, 224},
      {""}, {""}, {""},
#line 101 "svg-colours.gperf"
      {"mediumblue", 0, 0, 205},
      {""}, {""}, {""},
#line 126 "svg-colours.gperf"
      {"peachpuff", 255, 218, 185},
      {""},
#line 69 "svg-colours.gperf"
      {"greenyellow", 173, 255, 47},
      {""}, {""}, {""}, {""}, {""},
#line 14 "svg-colours.gperf"
      {"antiquewhite", 250, 235, 215},
      {""},
#line 22 "svg-colours.gperf"
      {"blue", 0, 0, 255},
#line 108 "svg-colours.gperf"
      {"mediumvioletred", 199, 21, 133},
      {""},
#line 103 "svg-colours.gperf"
      {"mediumpurple", 147, 112, 219},
      {""},
#line 65 "svg-colours.gperf"
      {"goldenrod", 218, 165, 32},
      {""}, {""}, {""}, {""},
#line 21 "svg-colours.gperf"
      {"blanchedalmond", 255, 235, 205},
#line 75 "svg-colours.gperf"
      {"khaki", 240, 230, 140},
      {""}, {""}, {""},
#line 129 "svg-colours.gperf"
      {"plum", 221, 160, 221},
      {""}, {""},
#line 102 "svg-colours.gperf"
      {"mediumorchid", 186, 85, 211},
      {""},
#line 133 "svg-colours.gperf"
      {"rosybrown", 188, 143, 143},
#line 105 "svg-colours.gperf"
      {"mediumslateblue", 123, 104, 238},
      {""},
#line 51 "svg-colours.gperf"
      {"darkturquoise", 0, 206, 209},
      {""}, {""}, {""}, {""}, {""},
#line 124 "svg-colours.gperf"
      {"palevioletred", 219, 112, 147},
      {""},
#line 125 "svg-colours.gperf"
      {"papayawhip", 255, 239, 213},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 106 "svg-colours.gperf"
      {"mediumspringgreen", 0, 250, 154},
#line 39 "svg-colours.gperf"
      {"darkgrey", 169, 169, 169},
      {""},
#line 107 "svg-colours.gperf"
      {"mediumturquoise", 72, 209, 204},
      {""}, {""},
#line 37 "svg-colours.gperf"
      {"darkgray", 169, 169, 169},
      {""}, {""}, {""}, {""},
#line 36 "svg-colours.gperf"
      {"darkgoldenrod", 184, 134, 11},
      {""}, {""}, {""},
#line 56 "svg-colours.gperf"
      {"dimgrey", 105, 105, 105},
      {""}, {""}, {""}, {""},
#line 55 "svg-colours.gperf"
      {"dimgray", 105, 105, 105},
#line 70 "svg-colours.gperf"
      {"honeydew", 240, 255, 240},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 18 "svg-colours.gperf"
      {"beige", 245, 245, 220},
      {""},
#line 151 "svg-colours.gperf"
      {"thistle", 216, 191, 216},
#line 31 "svg-colours.gperf"
      {"cornsilk", 255, 248, 220},
      {""},
#line 116 "svg-colours.gperf"
      {"olive", 128, 128, 0},
      {""}, {""}, {""}, {""},
#line 23 "svg-colours.gperf"
      {"blueviolet", 138, 43, 226},
      {""}, {""}, {""}, {""},
#line 100 "svg-colours.gperf"
      {"mediumaquamarine", 102, 205, 170},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 30 "svg-colours.gperf"
      {"cornflowerblue", 100, 149, 237},
      {""}, {""}, {""}, {""},
#line 13 "svg-colours.gperf"
      {"aliceblue", 240, 248, 255},
#line 130 "svg-colours.gperf"
      {"powderblue", 176, 224, 230},
      {""},
#line 123 "svg-colours.gperf"
      {"paleturquoise", 175, 238, 238},
      {""}, {""}, {""}, {""}, {""},
#line 50 "svg-colours.gperf"
      {"darkslategrey", 47, 79, 79},
#line 40 "svg-colours.gperf"
      {"darkkhaki", 189, 183, 107},
      {""}, {""}, {""},
#line 49 "svg-colours.gperf"
      {"darkslategray", 47, 79, 79},
#line 63 "svg-colours.gperf"
      {"ghostwhite", 248, 248, 255},
      {""}, {""}, {""}, {""},
#line 117 "svg-colours.gperf"
      {"olivedrab", 107, 142, 35},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 121 "svg-colours.gperf"
      {"palegoldenrod", 238, 232, 170},
      {""}, {""}, {""}, {""},
#line 35 "svg-colours.gperf"
      {"darkcyan", 0, 139, 139},
      {""}, {""}, {""},
#line 71 "svg-colours.gperf"
      {"hotpink", 255, 105, 180},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 62 "svg-colours.gperf"
      {"gainsboro", 220, 220, 220},
      {""}, {""}, {""},
#line 53 "svg-colours.gperf"
      {"deeppink", 255, 20, 147},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""},
#line 32 "svg-colours.gperf"
      {"crimson", 220, 20, 60},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 25 "svg-colours.gperf"
      {"burlywood", 222, 184, 135},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 59 "svg-colours.gperf"
      {"floralwhite", 255, 250, 240},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 156 "svg-colours.gperf"
      {"white", 255, 255, 255},
#line 113 "svg-colours.gperf"
      {"navajowhite", 255, 222, 173},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 158 "svg-colours.gperf"
      {"yellow", 255, 255, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 159 "svg-colours.gperf"
      {"yellowgreen", 154, 205, 50},
#line 90 "svg-colours.gperf"
      {"lightskyblue", 135, 206, 250},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 54 "svg-colours.gperf"
      {"deepskyblue", 0, 191, 255},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 157 "svg-colours.gperf"
      {"whitesmoke", 245, 245, 245}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register unsigned int key = hash (str, len);

      if (key <= MAX_HASH_VALUE)
        {
          register const char *s = wordlist[key].name;

          if ((((unsigned char)*str ^ (unsigned char)*s) & ~32) == 0 && !gperf_case_strcmp (str, s))
            return &wordlist[key];
        }
    }
  return 0;
}
#line 160 "svg-colours.gperf"


const struct svg_colour_t* svg_colour(const char *s)
{
  if (!s) return NULL;
  return svg_colours_lookup(s, strlen(s));
}
