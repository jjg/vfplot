#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/anynan.h"

#include <math.h>

bool anynan(size_t n, const double *z)
{
  for (size_t i = 0 ; i < n ; i++)
    {
      if (isnan(z[i]))
        return true;
    }

  return false;
}
