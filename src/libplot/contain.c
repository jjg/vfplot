#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/contain.h"

#include "plot/anynan.h"
#include "plot/gstack.h"
#include "plot/potential.h"
#include "plot/error.h"
#include "plot/scale.h"

#include <geom2d/sdcontact.h>
#include <log.h>

#include <rtree.h>

#include <stdlib.h>
#include <math.h>

/* truncation radius, compare with TRUNCATE_R0/1 in dim2.c */

#ifndef TRUNCATE_R
#define TRUNCATE_R 0.1
#endif

/* list of boundary line-segments, as endpoints */

typedef struct { vec_t u, v; } line_t;

typedef struct
{
  size_t n;
  line_t *lines;
} bls_t;

/* list of boundary line-segments, as semi-degenerate ellipses */

typedef struct
{
  size_t n;
  ellipse_t *ellipses;
} sdes_t;

/* as above, but in metric-tensor representation */

typedef struct
{
  mat_t mat;
  vec_t centre;
  double charge;
} sdmat_t;

typedef struct
{
  size_t n;
  sdmat_t *v;
} sdmats_t;

/* convert line segment to semi-degenerate ellipses */

static void line_ellipse(line_t line, ellipse_t *e)
{
  vec_t
    uv = vsub(line.v, line.u),
    centre = vadd(line.u, vscale(0.5, uv));
  double
    theta = vang(uv),
    major = vabs(uv) / 2;

  e->major = major;
  e->minor = 0;
  e->theta = theta;
  e->centre = centre;
}

/* convert line segment to rtree rectangle */

static void line_rect(line_t line, rtree_coord_t *rect)
{
  if (line.u.x < line.v.x)
    {
      rect[0] = line.u.x;
      rect[2] = line.v.x;
    }
  else
    {
      rect[0] = line.v.x;
      rect[2] = line.u.x;
    }

  if (line.u.y < line.v.y)
    {
      rect[1] = line.u.y;
      rect[3] = line.v.y;
    }
  else
    {
      rect[1] = line.v.y;
      rect[3] = line.u.y;
    }
}

/* split line and push segments onto the stack */

static int segments_stack(const line_t *line, gstack_t *stack, double max_length)
{
  vec_t uv = vsub(line->v, line->u);
  double length = vabs(uv);
  size_t n = ceil(length / max_length);

  if (n < 1)
    return 1;

  vec_t p[n + 1];

  for (size_t i = 0 ; i < n + 1 ; i++)
    p[i] = vadd(vscale((double)i / n, uv), line->u);

  int err = 0;

  for (size_t i = 0 ; i < n ; i++)
    {
      line_t segment = { .u = p[i], .v = p[i + 1] };
      err += gstack_push(stack, &segment);
    }

  return err;
}

/* callback to extract end-points from a part of a domain */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

typedef struct
{
  gstack_t *stack;
  double max_length;
} dom_stack_context_t;

static int dom_stack(polygon_t *p, void *arg, int m)
{
  dom_stack_context_t *context = arg;
  gstack_t *stack = context->stack;
  double max_length = context->max_length;
  size_t n = p->n;
  vec_t *v = p->v;

  if (n == 0)
    return 0;

  for (size_t i = 0 ; i < n ; i++)
    {
      size_t j = (i + 1) % n;
      line_t line = { .u = v[i], .v = v[j] };

      if (segments_stack(&line, stack, max_length) != 0)
        return 1;
    }

  return 0;
}

#pragma GCC diagnostic pop

/* convert stack of endpoint-pairs to bls_t* */

static bls_t* stack_bls(gstack_t *stack)
{
  size_t n = gstack_size(stack);

  if (n < 3)
    return NULL;

  bls_t *bls = NULL;
  line_t *lines;

  if ((lines = malloc(n * sizeof(line_t))) != NULL)
    {
      int err = 0;

      for (size_t i = 0 ; i < n ; i++)
        err += gstack_pop(stack, lines + i);

      if ((err == 0) && ((bls = malloc(sizeof(bls_t))) != NULL))
        {
          bls->lines = lines;
          bls->n = n;
        }
      else
        free(lines);
    }

  return bls;
}

static bls_t* bls_new(const domain_t *dom, double max_length)
{
  bls_t *bls = NULL;
  dom_stack_context_t context = { .max_length = max_length };

  if ((context.stack = gstack_new(sizeof(line_t), 64, 64)) != NULL)
    {
      if (domain_each((domain_t*)dom, dom_stack, &context) == 0)
        bls = stack_bls(context.stack);
      gstack_destroy(context.stack);
    }

  return bls;
}

static void bls_destroy(bls_t *bls)
{
  free(bls->lines);
  free(bls);
}

static rtree_t* bls_rtree(const bls_t *bls)
{
  rtree_t *rtree;

  if ((rtree = rtree_new(2, RTREE_DEFAULT)) != NULL)
    {
      size_t n = bls->n;
      line_t *lines = bls->lines;
      rtree_coord_t rect[4];
      int err = 0;

      for (size_t i = 0 ; i < n ; i++)
        {
          line_rect(lines[i], rect);
          err += rtree_add_rect(rtree, i, rect);
        }

      if (err == 0)
        return rtree;

      rtree_destroy(rtree);
    }

  return NULL;
}

static sdes_t* bls_sdes(const bls_t *bls)
{
  size_t n = bls->n;
  line_t *lines = bls->lines;
  sdes_t *sdes;

  if ((sdes = malloc(sizeof(sdes_t))) != NULL)
    {
      ellipse_t *ellipses;

      if ((ellipses = malloc(n * sizeof(ellipse_t))) != NULL)
        {
          for (size_t i = 0 ; i < n ; i++)
            line_ellipse(lines[i], ellipses + i);

          sdes->n = n;
          sdes->ellipses = ellipses;

          return sdes;
        }
      free(sdes);
    }

  return NULL;
}

static void sdes_destroy(sdes_t *sdes)
{
  free(sdes->ellipses);
  free(sdes);
}

/* convert semi-degenerate ellipse list into metric-tensor list */

static sdmats_t* sdes_sdmats(const sdes_t *sdes)
{
  sdmats_t *sdmats;

  if ((sdmats = malloc(sizeof(sdmats_t))) != NULL)
    {
      size_t n = sdes->n;
      sdmat_t *v;

      if ((v = malloc(sizeof(sdmat_t) * n)) != NULL)
        {
          for (size_t i = 0 ; i < sdes->n ; i++)
            {
              ellipse_t ellipse = sdes->ellipses[i];
              v[i].centre = ellipse.centre;
              v[i].charge = ellipse.major;
              ellipse_mat(&(ellipse), &(v[i].mat));
            }

          sdmats->v = v;
          sdmats->n = sdes->n;

          return sdmats;
        }
      free(sdmats);
    }

  return NULL;
}

static void sdmats_destroy(sdmats_t *sdmats)
{
  if (sdmats != NULL)
    free(sdmats->v);
  free(sdmats);
}

static contain_t* contain_alloc(const ellipse_mt_t *mt)
{
  size_t
    nx = ellipse_mt_nx(mt),
    ny = ellipse_mt_ny(mt);
  contain_t *contain;

  if ((contain = bilinear_new(nx, ny, 2)) != NULL)
    {
      bbox_t bbox = ellipse_mt_bbox(mt);
      bilinear_bbox_set(contain, bbox);
    }

  return contain;
}

static void contain_set(contain_t *contain, size_t i, size_t j, vec_t v)
{
  double z[2] = { v.x, v.y };
  bilinear_z_set(contain, i, j, z);
}

static double force(double d)
{
  return -potential_dV(d, TRUNCATE_R);
}

static void mat_rect(mat_t mu, vec_t u, rtree_coord_t *rect)
{
  double
    xmax = sqrt(mxmax(mu)),
    ymax = sqrt(mymax(mu));

  rect[0] = u.x - xmax;
  rect[1] = u.y - ymax;
  rect[2] = u.x + xmax;
  rect[3] = u.y + ymax;
}

/*
  The callback build_force() is passed to rtree_search, it is
  called with the ids of each of the sdmats (line-segment as a
  semi-degenerate ellipse in mat_t format) whose bounding rectangle
  intersect the bounding rectangle of the (non-degenerate) body
  ellipse also passed to rtree-search.  It is assumed that there are
  no duplicate ids k passed to this function, i.e., there is a 1-1
  correspondence of line segments and R-tree rectangles.
*/

typedef struct
{
  vec_t u;
  mat_t mu;
  double charge;
  const sdmats_t *sdmats;
  vec_t result;
} build_force_context_t;

static int build_force(rtree_id_t k, void *arg)
{
  build_force_context_t *context = arg;
  const sdmat_t *sdmat = context->sdmats->v + k;
  vec_t
    u = context->u,
    v = sdmat->centre,
    vu = vsub(u, v),
    w = {0};
  mat_t
    mu = context->mu,
    mv = sdmat->mat;
  double
    d,
    charge_u = context->charge,
    charge_v = sdmat->charge;

  /*
    In the case that the target ellipse has its centre on a boundary
    line-segment, then the direction of increase of P-W distance, w,
    is indeterminate; in this case it is assigned to zero, which means
    that this segment contributes nothing to the force (imagine a
    marble balanced on a razor blade).
  */

  if (sdcontact_mt(vu, mv, mu, &d, &w) != 0)
    {
      log_error("failed sd-contact (%g, %g)", vu.x, vu.y);
      return 1;
    }

  /*
    In the body dynamics, the result F is multiplied by the charges
    of the ellipses (and a constant), these charges are calculated
    in set_mq(), static in dim2.c, and is just the major axis of the
    ellipse multiplied by the charge schedule, that's a sine-spline
    going up to 1.  So that the boundary & body forces are comparible,
    we should do something similar here; this is constant for the
    lifetime of the program, that varying part will need to be multiplied
    in the dim2.c central loop.
  */

  double F = force(d) * charge_u * charge_v * SCALE_CONTAIN;

  context->result = vadd(vscale(F, w), context->result);

  return 0;
}

static int sdmats_contain(const sdmats_t *sdmats,
                          const rtree_t *rtree,
                          const ellipse_mt_t *mt,
                          const ellipse_major_t *major,
                          contain_t *contain)
{
  size_t
    nx = ellipse_mt_nx(mt),
    ny = ellipse_mt_ny(mt);

  build_force_context_t context = { .sdmats = sdmats };

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
        {
          mat_t mu;

          switch (ellipse_mt_mat_get(mt, i, j, &mu))
            {
            case ERROR_OK:
              break;
            case ERROR_NODATA:
              continue;
            default:
              return ERROR_BUG;
            }

          double charge = ellipse_major_get(major, i, j);

          if (isnan(charge))
            continue;

          double xy[2];
          bilinear_xy_get(contain, i, j, xy);

          vec_t u = { .x = xy[0], .y = xy[1] };

          rtree_coord_t rect[4];
          mat_rect(mu, u, rect);

          context.u = u;
          context.mu = mu;
          context.charge = charge;
          context.result.x = context.result.y = 0;

          if (rtree_search(rtree, rect, build_force, &context) != 0)
            {
              log_error("failed rtree_search");
              return 1;
            }

          contain_set(contain, i, j, context.result);
        }
    }

  return 0;
}

static contain_t* sdes_contain(const sdes_t *sdes,
                               const rtree_t *rtree,
                               const ellipse_mt_t *mt,
                               const ellipse_major_t *major)
{
  contain_t *contain = NULL;
  sdmats_t *sdmats;

  if ((sdmats = sdes_sdmats(sdes)) != NULL)
    {
      if ((contain = contain_alloc(mt)) != NULL)
        {
          if (sdmats_contain(sdmats, rtree, mt, major, contain) != 0)
            {
              contain_destroy(contain);
              contain = NULL;
            }
        }
      sdmats_destroy(sdmats);
    }

  return contain;
}

static contain_t* bls_contain(const bls_t *bls,
                              const ellipse_mt_t *mt,
                              const ellipse_major_t *major)
{
  contain_t *contain = NULL;
  sdes_t *sdes;

  if ((sdes = bls_sdes(bls)) != NULL)
    {
      rtree_t *rtree;

      if ((rtree = bls_rtree(bls)) != NULL)
        {
          contain = sdes_contain(sdes, rtree, mt, major);
          rtree_destroy(rtree);
        }

      sdes_destroy(sdes);
    }

  return contain;
}

contain_t* contain_new(const domain_t *dom,
                       const ellipse_mt_t *mt,
                       const ellipse_major_t *major,
                       double max_length)
{
  contain_t *contain = NULL;
  bls_t *bls;

  if ((bls = bls_new(dom, max_length)) != NULL)
    {
      contain = bls_contain(bls, mt, major);
      bls_destroy(bls);
    }

  return contain;
}

void contain_destroy(contain_t *contain)
{
  bilinear_destroy(contain);
}

int contain_eval(const contain_t *contain, double x, double y, vec_t *pF)
{
  double z[2];

  if (bilinear_eval(contain, x, y, z) != 0)
    return ERROR_BUG;

  if (anynan(2, z))
    return ERROR_NODATA;

  *pF = (vec_t){ .x = z[0], .y = z[1] };

  return ERROR_OK;
}
