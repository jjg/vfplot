#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/adaptive.h"

#include "plot/contain.h"
#include "plot/dim2.h"
#include "plot/evaluate.h"
#include "plot/limits.h"
#include "plot/status.h"
#include "plot/ellipse-mt.h"
#include "plot/ellipse-density.h"

#include <geom2d/mat.h>

#include <log.h>

#include <math.h>
#include <stdlib.h>

static int plot_contain(dim2_opt_t *d2opt,
                        arrows_t *A,
                        nbrs_t *N)
{
  status_set_length(4, 12);

  log_info("dimension two");

  int err;

  if ((err = dim2(d2opt, A, N)) != ERROR_OK)
    log_error("failed at dimension two");
  else
    status("final", A->n);

  return err;
}

static int plot_mt(dim2_opt_t *d2opt,
                   ellipse_base_t *base,
                   arrows_t *A,
                   nbrs_t *N)
{
  double A_dom = domain_area(d2opt->dom);

  log_info("domain area %.3g", A_dom);

  /*
    I_den is the integral of the ellipse density (the reciprocal
    of the area) over the domaim, mean_den the mean density
  */

  double mean_den;

  {
    ellipse_density_t *density;

    if ((density = ellipse_density_new(base)) == NULL)
      {
        log_error("failed to create ellipse-density");
        return ERROR_NODATA;
      }

    double I_den;
    int err = ellipse_density_integrate(density, d2opt->dom, &I_den);

    ellipse_density_destroy(density);

    if (err != ERROR_OK)
      {
        log_error("failed to integrate ellipse density");
        return err;
      }

    if (! (I_den > 0.0) )
      {
        log_error("non-positive ellipse-density integral, bad field?");
        return ERROR_NODATA;
      }

    mean_den = I_den / A_dom;
  }

  log_info("mean ellipse density %.3g (estimate)", mean_den);

  contain_t *contain = NULL;
  ellipse_major_t *major;
  int err = ERROR_OK;

  if ((major = ellipse_major_new(base)) != NULL)
    {
      double I_maj;
      err = ellipse_major_integrate(major, d2opt->dom, &I_maj);

      if (err == ERROR_OK)
        {
          if (I_maj > 0.0)
            {
              double mean_maj = I_maj / A_dom;
              contain = contain_new(d2opt->dom, d2opt->mt, major, 2 * mean_maj);
            }
          else
            {
              log_error("non-positive ellipse-major integral, bad field?");
              err = ERROR_NODATA;
            }
        }
      else
        log_error("failed to integrate ellipse major");

      ellipse_major_destroy(major);
    }
  else
    {
      log_error("failed to create ellipse-major");
      err = ERROR_MALLOC;
    }

  if (err != ERROR_OK)
    return err;

  if (contain == NULL)
    {
      log_error("failed to create containment field");
      return ERROR_USER;
    }

  d2opt->density = mean_den;
  d2opt->contain = contain;

  err = plot_contain(d2opt, A, N);

  d2opt->density = 0;
  d2opt->contain = NULL;
  contain_destroy(contain);

  return err;
}

static int plot_base(dim2_opt_t *d2opt,
                     ellipse_base_t *base,
                     arrows_t *A,
                     nbrs_t *N)
{
  ellipse_mt_t *mt = ellipse_mt_new(base);

  if (mt == NULL)
    {
      log_error("failed to create metric tensor");
      return ERROR_NODATA;
    }

  d2opt->mt = mt;

  int err = plot_mt(d2opt, base, A, N);

  d2opt->mt = NULL;
  ellipse_mt_destroy(mt);

  return err;
}

int plot_adaptive(const domain_t *dom,
                  vfun_t *fv, cfun_t *fc, void *field,
                  plot_opt_t *opt,
                  arrows_t *A,
                  nbrs_t *N)
{
  log_info("adaptive placement");

  evaluate_t evaluate = {
    .fv = fv,
    .fc = fc,
    .field = field,
    .aspect = opt->arrow.aspect
  };

  log_info("arrow margins %.2f pt, %.2f pt, rate %.2f",
	   opt->place.adaptive.margin.major,
	   opt->place.adaptive.margin.minor,
	   opt->place.adaptive.margin.rate);
  log_info("page scaling %.2f", opt->page.scale);

  bbox_t bb = opt->bbox;
  double w = bbox_width(&bb), h = bbox_height(&bb);
  size_t nx, ny, mtc = opt->place.adaptive.mtcache;

  if (w < h)
    {
      nx = mtc;
      ny = mtc * h / w;
    }
  else
    {
      nx = mtc * w / h;
      ny = mtc;
    }

  arrow_opt_t arrow_opt = {
    .M = opt->place.adaptive.margin.rate,
    .bmaj = opt->place.adaptive.margin.major,
    .bmin = opt->place.adaptive.margin.minor,
    .scale = opt->page.scale
  };

  log_info("caching %zi x %zi ellipse-base", nx, ny);

  ellipse_base_t *base = ellipse_base_new(bb, nx, ny, &evaluate, &arrow_opt);

  if (base == NULL)
    {
      log_error("failed to create ellipse-base");
      return ERROR_NODATA;
    }

  dim2_opt_t d2opt = {
    .plot = *opt,
    .density = 0,
    .dom = dom,
    .mt = NULL,
    .evaluate = &evaluate,
    .contain = NULL,
    .arrow_opt = &arrow_opt
  };

  int err = plot_base(&d2opt, base, A, N);

  ellipse_base_destroy(base);

  return err;
}
