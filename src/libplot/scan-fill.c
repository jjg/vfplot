#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/scan-fill.h"
#include "plot/scan-colour.h"
#include "plot/error.h"

const fill_t scan_fill_default = { .type = fill_none };

int scan_fill(const char *arg, fill_t *fill)
{
  fill->type = fill_colour;
  int err = scan_colour(arg, &(fill->colour));
  return err;
}
