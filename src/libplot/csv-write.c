#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/csv-write.h"

#include "plot/macros.h"
#include "plot/error.h"

#include <geom2d/sincos.h>
#include <log.h>

#include <math.h>
#include <stdlib.h>

int csv_write(const char *file,
              const domain_t *dom,
              vfun_t *fv,
              void *field,
              size_t n, size_t m)
{
  bbox_t bb;
  int err;

  if ((err = domain_bbox(dom, &bb)) != ERROR_OK)
    return err;

  double
    w = bbox_width(&bb),
    h = bbox_height(&bb),
    x0 = bb.x.min,
    y0 = bb.y.min;

  if (n * m == 0)
    {
      log_error("empty %zix%zi grid for dump", n, m);
      return ERROR_BUG;
    }

  FILE *st = fopen(file, "w");

  if (!st)
    {
      log_error("failed to open %s", file);
      return ERROR_WRITE_OPEN;
    }

  /* we shave a pixel here, it could be less FIXME */

  double
    dx = w / n,
    dy = h / m;

  for (size_t i = 0 ; i < n ; i++)
    {
      double x = x0 + (i + 0.5) * dx;

      for (size_t j = 0 ; j < m ; j++)
	{
	  double y = y0 + (j + 0.5) * dy;
	  vec_t v = {x, y};

	  if (! domain_inside(dom, v))
            continue;

          double t, m;

	  /* fv is non-zero for nodata */

	  if (fv(field, x, y, &t, &m) == 0)
            {
              double sint, cost;
              sincos(t, &sint, &cost);
              fprintf(st, "%zi,%zi,%g,%g\n", i, j, m * cost, m * sint);
            }
	}
    }

  fclose(st);

  return ERROR_OK;
}
