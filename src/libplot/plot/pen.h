/*
  pen.h
  Copyright (c) J.J. Green 2023
*/

#ifndef PLOT_PEN_H
#define PLOT_PEN_H

#include <plot/colour.h>

typedef struct
{
  double width;
  colour_t colour;
} pen_t;

#endif
