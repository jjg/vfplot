#ifndef PLOT_CURVATURE_H
#define PLOT_CURVATURE_H

#include <plot/vfun.h>

int curvature(vfun_t*, void*, double, double, double, double*);

#endif
