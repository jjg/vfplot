#ifndef PLOT_PAGE_H
#define PLOT_PAGE_H

#include <plot/bbox.h>

typedef enum
{
  specify_height,
  specify_width
} page_type_t;

typedef struct
{
  page_type_t type;
  double width, height, scale;
} page_t;

int page_complete(const bbox_t*, page_t*);

#endif
