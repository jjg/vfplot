#ifndef PLOT_CSV_WRITE_H
#define PLOT_CSV_WRITE_H

#include <plot/vfun.h>
#include <plot/domain.h>

#include <stddef.h>

int csv_write(const char*, const domain_t*, vfun_t*, void*, size_t, size_t);

#endif
