/*
  scan-pen.h
  Copyright (c) J.J. Green 2023
*/

#ifndef PLOT_SCAN_PEN_H
#define PLOT_SCAN_PEN_H

#include <plot/pen.h>

extern const pen_t scan_pen_default;

int scan_pen(const char*, pen_t*);

#endif
