#ifndef PLOT_COLOUR_H
#define PLOT_COLOUR_H

typedef enum { colour_none, colour_grey, colour_rgb } colour_type_t;

typedef unsigned char grey_t;
typedef struct { unsigned char r, g, b; } rgb_t;

typedef struct
{
  colour_type_t type;
  union
  {
    grey_t grey;
    rgb_t rgb;
  };
} colour_t;

#endif
