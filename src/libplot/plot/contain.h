#ifndef PLOT_CONTAIN_H
#define PLOT_CONTAIN_H

#include <plot/domain.h>
#include <plot/bilinear.h>
#include <plot/ellipse-mt.h>
#include <plot/ellipse-major.h>

typedef bilinear_t contain_t;

contain_t* contain_new(const domain_t*,
                       const ellipse_mt_t*,
                       const ellipse_major_t*,
                       double);
int contain_eval(const contain_t*, double, double, vec_t*);
void contain_destroy(contain_t*);

#endif
