#ifndef PLOT_ARROW_H
#define PLOT_ARROW_H

#include <geom2d/ellipse.h>
#include <geom2d/vec.h>

/*
  x, y   : midpoint of the line between the shaft endpoints
  theta  : direction
  length : length of curved shaft
  radius : radius of curvature (positive)
  bend   : direction of curvature
*/

typedef struct
{
  vec_t centre;
  bend_t bend;
  double theta, length, width, curv;
} arrow_t;

typedef struct
{
  double M, bmaj, bmin, scale;
} arrow_opt_t;

void arrow_ellipse(const arrow_t*, const arrow_opt_t*, ellipse_t*);
arrow_t arrow_translate(arrow_t, vec_t);
arrow_t arrow_rotate(arrow_t, double);
arrow_t arrow_scale(arrow_t, double);

#endif
