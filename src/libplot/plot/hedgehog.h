#ifndef PLOT_HEDGEHOG_H
#define PLOT_HEDGEHOG_H

#include <plot/plot.h>
#include <plot/arrows.h>

int plot_hedgehog(const domain_t*,
                  vfun_t*, cfun_t*, void*,
                  plot_opt_t*,
                  arrows_t*);

#endif
