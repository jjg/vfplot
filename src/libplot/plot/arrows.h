#ifndef PLOT_ARROWS_H
#define PLOT_ARROWS_H

#include <plot/arrow.h>
#include <stdint.h>

typedef struct
{
  size_t n;
  arrow_t *v;
} arrows_t;

#endif
