#ifndef PLOT_EVALUATE_H
#define PLOT_EVALUATE_H

#include <plot/arrow.h>
#include <plot/vfun.h>

typedef struct
{
  vfun_t *fv;
  cfun_t *fc;
  void *field;
  double aspect;
} evaluate_t;

int complete_arrow(const evaluate_t*, arrow_t*);

#endif
