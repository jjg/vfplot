#ifndef PLOT_NBRS_H
#define PLOT_NBRS_H

#include <plot/nbr.h>
#include <stdint.h>

typedef struct
{
  size_t n;
  nbr_t *v;
} nbrs_t;

#endif
