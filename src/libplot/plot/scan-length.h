/*
  scan-length.h
  Copyright (c) J.J. Green 2023
*/

#ifndef PLOT_SCAN_LENGTH_H
#define PLOT_SCAN_LENGTH_H

int scan_length(const char*, const char*, double*);

#endif
