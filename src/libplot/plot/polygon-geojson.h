#ifndef PLOT_POLYGON_GEOJSON_H
#define PLOT_POLYGON_GEOJSON_H

#include <plot/polygon.h>

/*
  if jannson.h is not included, this provides a partial of
  the types needed by the geojson methods, and in that case
  the functions never dereference the type, so it's not an
  error to not fully define it.  When jannson.h is included
  this constitutes a "double typedef", only permitteded since
  C11, so we might need some work on this.  I asked on SO:
  https://stackoverflow.com/questions/74621516/
*/

typedef struct json_t json_t;

json_t* polygon_geojson_serialise(const polygon_t*);
int polygon_geojson_deserialise(const json_t*, polygon_t*);

#endif
