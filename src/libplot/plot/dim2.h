#ifndef PLOT_DIM2_H
#define PLOT_DIM2_H

#include <plot/domain.h>
#include <plot/contain.h>
#include <plot/arrows.h>
#include <plot/nbrs.h>
#include <plot/ellipse-mt.h>
#include <plot/evaluate.h>
#include <plot/plot.h>

#include <geom2d/ellipse.h>

typedef struct
{
  plot_opt_t plot;
  double density;
  const domain_t *dom;
  ellipse_mt_t *mt;
  evaluate_t *evaluate;
  contain_t *contain;
  arrow_opt_t *arrow_opt;
} dim2_opt_t;

int dim2(dim2_opt_t*, arrows_t*, nbrs_t*);

#endif
