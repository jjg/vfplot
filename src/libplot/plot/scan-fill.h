/*
  scan-fill.h
  Copyright (c) J.J. Green 2023
*/

#ifndef PLOT_SCAN_FILL_H
#define PLOT_SCAN_FILL_H

#include <plot/fill.h>

int scan_fill(const char*, fill_t*);

extern const fill_t scan_fill_default;

#endif
