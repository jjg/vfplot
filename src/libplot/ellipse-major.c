#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/ellipse-major.h"

#include "plot/arrow.h"
#include "plot/evaluate.h"
#include "plot/error.h"

#include <geom2d/ellipse.h>
#include <geom2d/vec.h>

#include <math.h>

ellipse_major_t* ellipse_major_new(const ellipse_base_t *base)
{
  size_t
    nx = ellipse_base_nx(base),
    ny = ellipse_base_ny(base);
  ellipse_major_t *major;

  if ((major = bilinear_new(nx, ny, 1)) == NULL)
    return NULL;

  bbox_t bbox = ellipse_base_bbox(base);

  bilinear_bbox_set(major, bbox);

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
	{
          ellipse_t E;
          int err = ellipse_base_get(base, i, j, &E);

	  switch (err)
	    {
	    case ERROR_OK:
              bilinear_z_set(major, i, j, &(E.major));
              break;

            case ERROR_NODATA:
              break;

            default:
              bilinear_destroy(major);
              return NULL;
            }
        }
    }

  return major;
}

int ellipse_major_integrate(const ellipse_major_t *major,
                           const domain_t *dom,
                           double *I)
{
  double Im[1];
  int err;

  if ((err = bilinear_integrate(major, dom, Im)) == 0)
    *I = Im[0];

  return err;
}

void ellipse_major_destroy(ellipse_major_t *major)
{
  bilinear_destroy(major);
}

double ellipse_major_get(const ellipse_major_t *major, size_t i, size_t j)
{
  double M;
  bilinear_z_get(major, i, j, &M);
  return M;
}
