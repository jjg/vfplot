#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/scan-crop.h"
#include "plot/scan-length.h"
#include "plot/error.h"

#include <log.h>

#include <stdio.h>
#include <stdlib.h>

const crop_t scan_crop_default = { .type = crop_none };

/* crop is 1, 2, or 4 lengths */

int scan_crop(const char *p, crop_t *crop)
{
  const char fmt[] = "%m[^/]/%m[^/]/%m[^/]/%m[^/]";
  char *s1, *s2, *s3, *s4;
  double W, E, S, N;
  int err;

  switch (sscanf(p, fmt, &s1, &s2, &s3, &s4))
    {
    case 1:
      if ((err = scan_length(s1, "crop", &W)) != ERROR_OK)
        return err;
      free(s1);
      E = S = N = W;
      break;

    case 2:
      if ((err = scan_length(s1, "crop", &W)) != ERROR_OK)
        return err;
      free(s1);
      E = W;
      if ((err = scan_length(s2, "crop", &S)) != ERROR_OK)
        return err;
      free(s2);
      N = S;
      break;

    case 3:
      free(s1); free(s2); free(s3);
      log_error("malformed --crop argument: %s", p);
      return ERROR_USER;

    case 4:
      if ((err = scan_length(s1, "crop", &W)) != ERROR_OK)
        return err;
      free(s1);
      if ((err = scan_length(s2, "crop", &E)) != ERROR_OK)
        return err;
      free(s2);
      if ((err = scan_length(s3, "crop", &S)) != ERROR_OK)
        return err;
      free(s3);
      if ((err = scan_length(s4, "crop", &N)) != ERROR_OK)
        return err;
      free(s4);
      break;

    default:
      return ERROR_BUG;
    }

  crop->type = crop_simple;
  crop->x.min = W;
  crop->x.max = E;
  crop->y.min = S;
  crop->y.max = N;

  return ERROR_OK;
}
