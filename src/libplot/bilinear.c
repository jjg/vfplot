#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/bilinear.h"
#include "plot/anynan.h"
#include "plot/bbox.h"
#include "plot/domain-build.h"

#include <geom2d/vec.h>
#include <geom2d/mat.h>

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

struct bilinear_t
{
  struct {
    size_t x, y, z;
  } n;
  bbox_t bbox;
  double *v;
};

bilinear_t* bilinear_new(size_t nx, size_t ny, size_t nz)
{
  bilinear_t *B;

  if ((B = malloc(sizeof(bilinear_t))) != NULL)
    {
      size_t n = nx * ny * nz;
      double *v;

      if ((v = calloc(n, sizeof(double))) != NULL)
        {
          for (size_t i = 0 ; i < n ; i++)
            v[i] = NAN;

          B->n.x = nx;
          B->n.y = ny;
          B->n.z = nz;
          B->v = v;

          return B;
        }
      free(B);
    }
  return NULL;
}

void bilinear_destroy(bilinear_t *B)
{
  if (B != NULL)
    free(B->v);
  free(B);
}

void bilinear_bbox_set(bilinear_t *B, bbox_t bbox)
{
  B->bbox = bbox;
}

bbox_t bilinear_bbox_get(const bilinear_t *B)
{
  return B->bbox;
}

size_t bilinear_nx_get(const bilinear_t *B)
{
  return B->n.x;
}

size_t bilinear_ny_get(const bilinear_t *B)
{
  return B->n.y;
}

size_t bilinear_nz_get(const bilinear_t *B)
{
  return B->n.z;
}

void bilinear_xy_get(const bilinear_t *B, size_t i, size_t j, double xy[2])
{
  size_t
    nx = bilinear_nx_get(B),
    ny = bilinear_ny_get(B);
  bbox_t
    bbox = bilinear_bbox_get(B);

  xy[0] = (i * bbox.x.max + (nx - 1 - i) * bbox.x.min) / (nx - 1);
  xy[1] = (j * bbox.y.max + (ny - 1 - j) * bbox.y.min) / (ny - 1);
}

/* index of tuple, multiply by arity to get location in double array */

static size_t tuple_index(const bilinear_t *B, size_t i, size_t j)
{
  return j * bilinear_nx_get(B) + i;
}

/*
  we allow integer indices here, since we want to represent large
  grid of points extending in all directions (truncation of putative
  (x, y) pairs would give those), even though we can only interpolate
  on that subgrid rectangle of non-negative integers.

  this should be called prior to bilinear_z_get etc. if there
  is any possibility that those indices might be out of bounds, but
  one does not need to do that in manually iterating over the grid
  in function this module, that would be wasted effort.
*/

bool bilinear_valid(const bilinear_t *B, int i, int j)
{
  return
    (0 <= i) && ((size_t)i < bilinear_nx_get(B)) &&
    (0 <= j) && ((size_t)j < bilinear_ny_get(B));
}

void bilinear_z_get(const bilinear_t *B, size_t i, size_t j, double *z)
{
  size_t nz = bilinear_nz_get(B);
  double *tuple = B->v + (tuple_index(B, i, j) * nz);
  memcpy(z, tuple, nz * sizeof(double));
}

void bilinear_z_set(bilinear_t *B, size_t i, size_t j, const double *z)
{
  size_t nz = bilinear_nz_get(B);
  double *tuple = B->v + (tuple_index(B, i, j) * nz);
  memcpy(tuple, z, nz * sizeof(double));
}

int bilinear_each(bilinear_t *B, bilinear_iterator_t *f, void *arg)
{
  size_t
    nx = bilinear_nx_get(B),
    ny = bilinear_ny_get(B);

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
        {
          int err;

          if ((err = f(B, i, j, arg)) != 0)
            return err;
        }
    }

  return 0;
}

static int csv_write(bilinear_t *B, size_t i, size_t j, void *arg)
{
  size_t nz = bilinear_nz_get(B);
  double z[nz];
  double xy[2];

  bilinear_xy_get(B, i, j, xy);
  bilinear_z_get(B, i, j, z);

  if (! anynan(nz, z))
    {
      FILE *stream = arg;

      fprintf(stream, "%g,%g", xy[0], xy[1]);

      for (size_t k = 0 ; k < nz ; k++)
        fprintf(stream, ",%g", z[k]);

      fprintf(stream, "\r\n");
    }

  return 0;
}

int bilinear_csv_write(const bilinear_t *B, FILE *stream)
{
  return bilinear_each((bilinear_t*)B, csv_write, stream);
}

/* helper for eval */

static void zij(const bilinear_t *B, int i, int j, double *z)
{
  if (bilinear_valid(B, i, j))
    bilinear_z_get(B, i, j, z);
  else
    {
      for (size_t k = 0 ; k < bilinear_nz_get(B) ; k++)
        z[k] = NAN;
    }
}

/*
  This was bilinear() in the earlier scalar implementation, that
  behaved a little differently retruning an OK/NODATA/ERROR value,
  but since we might have some NODATA values, that would be a bit
  awkward, so now we just have an OK/ERROR return value, and the
  caller should inspect z[] for NANs if they care about that.
*/

int bilinear_eval(const bilinear_t *B, double x, double y, double *z)
{
  bbox_t bbox = bilinear_bbox_get(B);
  size_t
    nx = bilinear_nx_get(B),
    ny = bilinear_ny_get(B),
    nz = bilinear_nz_get(B);
  double
    x0 = (nx - 1) * (x - bbox.x.min) / (bbox.x.max - bbox.x.min),
    y0 = (ny - 1) * (y - bbox.y.min) / (bbox.y.max - bbox.y.min);
  int
    i = floor(x0),
    j = floor(y0);
  double
    X = x0 - i,
    Y = y0 - j,
    z00[nz],
    z01[nz],
    z10[nz],
    z11[nz];

  zij(B, i, j, z00);
  zij(B, i, j + 1, z01);
  zij(B, i + 1, j, z10);
  zij(B, i + 1, j + 1, z11);

  for (size_t k = 0 ; k < nz ; k++)
    {
      double
        z00k = z00[k],
        z01k = z01[k],
        z10k = z10[k],
        z11k = z11[k];
      size_t nan_count =
        (isnan(z00k) ? 1 : 0) +
        (isnan(z01k) ? 1 : 0) +
        (isnan(z10k) ? 1 : 0) +
        (isnan(z11k) ? 1 : 0);

      z[k] = NAN;

      switch (nan_count)
        {
        case 0:

          z[k] =
            (z00k * (1 - X) + z10k * X) * (1 - Y) +
            (z01k * (1 - X) + z11k * X) * Y;
          break;

        case 1:

          if (isnan(z11k))
            {
              if (X + Y < 1)
                z[k] =
                  (z10k - z00k) * X +
                  (z01k - z00k) * Y +
                  z00k;
            }
          else if (isnan(z01k))
            {
              if (X > Y)
                z[k] =
                  (z10k - z00k) * X +
                  (z11k - z10k) * Y +
                  z00k;
            }
          else if (isnan(z10k))
            {
              if (X < Y)
                z[k] =
                  (z11k - z01k) * X +
                  (z01k - z00k) * Y +
                  z00k;
            }
          else
            {
              if (X + Y > 1)
                z[k] =
                  (z11k - z01k) * X +
                  (z11k - z10k) * Y +
                  z10k + z01k - z11k;
            }

          break;

        case 2:
        case 3:
        case 4:

          break;

        default:

          return 1;
        }
    }

  return 0;
}

static double bilinear_dx(const bilinear_t *B)
{
  double w = bbox_width(&(B->bbox));
  size_t nx = bilinear_nx_get(B);

  return w / (nx - 1);
}

static double bilinear_dy(const bilinear_t *B)
{
  double h = bbox_height(&(B->bbox));
  size_t ny = bilinear_ny_get(B);

  return h / (ny - 1);
}

static vec_t vecij(const bilinear_t *B, size_t i, size_t j)
{
  double z[2];
  bilinear_z_get(B, i, j, z);
  vec_t v = {z[0], z[1]};
  return v;
}

/*
  nandif(a, b, c) returns one of c - b,  b - a or (c-a)/2 if the result
  can be non-nan (ie, if at least 2 of them are non-nan) otherwise it
  returns nan
*/

static double nandif(double a, double b, double c)
{
  if (isnan(a))
    return ((isnan(b) || isnan(c)) ? NAN : c - b);

  if (isnan(c))
    return (isnan(b) ? NAN : b - a);

  return (c - a) / 2;
}

/*
  returns a newly allocated bilinear_t which holds the curvature of the
  input: the value is Ju where u is the unit vector field and J the
  Jacobian of u = (u, v)

    U = [ du/dx du/dy ]
        [ dv/dx dv/dy ]

  We treat the interior in the obvious way, but the boundary of the
  field a little differently, this is to avoid pixel wide boundary of
  no-data (1.5 for pixel-aligned) which was the subject to the issue
  https://gitlab.com/jjg/vfplot/-/issues/117
*/

bilinear_t* bilinear_curvature(const bilinear_t *B)
{
  if (bilinear_nz_get(B) != 2)
    return NULL;

  size_t
    nx = bilinear_nx_get(B),
    ny = bilinear_ny_get(B);
  bilinear_t *Bc;

  if ((Bc = bilinear_new(nx, ny, 1)) == NULL)
    return NULL;

  bilinear_bbox_set(Bc, bilinear_bbox_get(B));

  double
    dx = bilinear_dx(B),
    dy = bilinear_dy(B);

  /* interior */

  for (size_t i = 1 ; i < nx - 1 ; i++)
    {
      for (size_t j = 1 ; j < ny - 1 ; j++)
        {
          vec_t
            v0 = vecij(B, i, j),
            vt = vecij(B, i, j + 1),
            vb = vecij(B, i, j - 1),
            vl = vecij(B, i - 1, j),
            vr = vecij(B, i + 1, j);

          vec_t
            u0 = vunit(v0),
            ut = vunit(vt),
            ub = vunit(vb),
            ul = vunit(vl),
            ur = vunit(vr);

          double
            dudx = nandif(ul.x, u0.x, ur.x) / dx,
            dudy = nandif(ub.x, u0.x, ut.x) / dy,
            dvdx = nandif(ul.y, u0.y, ur.y) / dx,
            dvdy = nandif(ub.y, u0.y, ut.y) / dy;

          mat_t M = {dudx, dudy, dvdx, dvdy};
          vec_t vk = mvmul(M, u0);
          double k = (bend2v(u0, vk) == rightward ? 1 : -1) * vabs(vk);

          bilinear_z_set(Bc, i, j, &k);
        }
    }

  /* the lines of the boundary */

  for (size_t i = 1 ; i < nx - 1; i++)
    {
      vec_t
        v0 = vecij(B, i, 0),
        vt = vecij(B, i, 1),
        vl = vecij(B, i - 1, 0),
        vr = vecij(B, i + 1, 0);

      vec_t
        u0 = vunit(v0),
        ut = vunit(vt),
        ul = vunit(vl),
        ur = vunit(vr);

      double
        dudx = nandif(ul.x, u0.x, ur.x) / dx,
        dudy = nandif(NAN, u0.x, ut.x) / dy,
        dvdx = nandif(ul.y, u0.y, ur.y) / dx,
        dvdy = nandif(NAN, u0.y, ut.y) / dy;

      mat_t M = {dudx, dudy, dvdx, dvdy};
      vec_t vk = mvmul(M, u0);
      double k = (bend2v(u0, vk) == rightward ? 1 : -1) * vabs(vk);

      bilinear_z_set(Bc, i, 0, &k);
    }

  for (size_t i = 1 ; i < nx - 1 ; i++)
    {
      vec_t
        v0 = vecij(B, i, ny - 1),
        vb = vecij(B, i, ny - 2),
        vl = vecij(B, i - 1, ny - 1),
        vr = vecij(B, i + 1, ny - 1);

      vec_t
        u0 = vunit(v0),
        ub = vunit(vb),
        ul = vunit(vl),
        ur = vunit(vr);

      double
        dudx = nandif(ul.x, u0.x, ur.x) / dx,
        dudy = nandif(ub.x, u0.x, NAN) / dy,
        dvdx = nandif(ul.y, u0.y, ur.y) / dx,
        dvdy = nandif(ub.y, u0.y, NAN) / dy;

      mat_t M = {dudx, dudy, dvdx, dvdy};
      vec_t vk = mvmul(M, u0);
      double k = (bend2v(u0, vk) == rightward ? 1 : -1) * vabs(vk);

      bilinear_z_set(Bc, i, ny - 1, &k);
    }

  for (size_t j = 1 ; j < ny - 1 ; j++)
    {
      vec_t
        v0 = vecij(B, 0, j),
        vt = vecij(B, 0, j + 1),
        vb = vecij(B, 0, j - 1),
        vr = vecij(B, 1, j);

      vec_t
        u0 = vunit(v0),
        ut = vunit(vt),
        ub = vunit(vb),
        ur = vunit(vr);

      double
        dudx = nandif(NAN, u0.x, ur.x) / dx,
        dudy = nandif(ub.x, u0.x, ut.x) / dy,
        dvdx = nandif(NAN, u0.y, ur.y) / dx,
        dvdy = nandif(ub.y, u0.y, ut.y) / dy;

      mat_t M = {dudx, dudy, dvdx, dvdy};
      vec_t vk = mvmul(M, u0);
      double k = (bend2v(u0, vk) == rightward ? 1 : -1) * vabs(vk);

      bilinear_z_set(Bc, 0, j, &k);
    }

  for (size_t j = 1 ; j < ny - 1 ; j++)
    {
      vec_t
        v0 = vecij(B, nx - 1, j),
        vt = vecij(B, nx - 1, j + 1),
        vb = vecij(B, nx - 1, j - 1),
        vl = vecij(B, nx - 2, j);

      vec_t
        u0 = vunit(v0),
        ut = vunit(vt),
        ub = vunit(vb),
        ul = vunit(vl);

      double
        dudx = nandif(ul.x, u0.x, NAN) / dx,
        dudy = nandif(ub.x, u0.x, ut.x) / dy,
        dvdx = nandif(ul.y, u0.y, NAN) / dx,
        dvdy = nandif(ub.y, u0.y, ut.y) / dy;

      mat_t M = {dudx, dudy, dvdx, dvdy};
      vec_t vk = mvmul(M, u0);
      double k = (bend2v(u0, vk) == rightward ? 1 : -1) * vabs(vk);

      bilinear_z_set(Bc, nx - 1, j, &k);
    }

  /* the corners, this is probably overkill, but cheap at least  */

  {
    vec_t
      v0 = vecij(B, 0, 0),
      vt = vecij(B, 0, 1),
      vr = vecij(B, 1, 0);

    vec_t
      u0 = vunit(v0),
      ut = vunit(vt),
      ur = vunit(vr);

    double
      dudx = nandif(NAN, u0.x, ur.x) / dx,
      dudy = nandif(NAN, u0.x, ut.x) / dy,
      dvdx = nandif(NAN, u0.y, ur.y) / dx,
      dvdy = nandif(NAN, u0.y, ut.y) / dy;

    mat_t M = {dudx, dudy, dvdx, dvdy};
    vec_t vk = mvmul(M, u0);
    double k = (bend2v(u0, vk) == rightward ? 1 : -1) * vabs(vk);

    bilinear_z_set(Bc, 0, 0, &k);
  }

  {
    vec_t
      v0 = vecij(B, 0, ny - 1),
      vb = vecij(B, 0, ny - 2),
      vr = vecij(B, 1, ny - 1);

    vec_t
      u0 = vunit(v0),
      ub = vunit(vb),
      ur = vunit(vr);

    double
      dudx = nandif(NAN, u0.x, ur.x) / dx,
      dudy = nandif(ub.x, u0.x, NAN) / dy,
      dvdx = nandif(NAN, u0.y, ur.y) / dx,
      dvdy = nandif(ub.y, u0.y, NAN) / dy;

    mat_t M = {dudx, dudy, dvdx, dvdy};
    vec_t vk = mvmul(M, u0);
    double k = (bend2v(u0, vk) == rightward ? 1 : -1) * vabs(vk);

    bilinear_z_set(Bc, 0, ny - 1, &k);
  }

  {
    vec_t
      v0 = vecij(B, nx - 1, 0),
      vt = vecij(B, nx - 1, 1),
      vl = vecij(B, nx - 2, 0);

    vec_t
      u0 = vunit(v0),
      ut = vunit(vt),
      ul = vunit(vl);

    double
      dudx = nandif(ul.x, u0.x, NAN) / dx,
      dudy = nandif(NAN, u0.x, ut.x) / dy,
      dvdx = nandif(ul.y, u0.y, NAN) / dx,
      dvdy = nandif(NAN, u0.y, ut.y) / dy;

    mat_t M = {dudx, dudy, dvdx, dvdy};
    vec_t vk = mvmul(M, u0);
    double k = (bend2v(u0, vk) == rightward ? 1 : -1) * vabs(vk);

    bilinear_z_set(Bc, nx - 1, 0, &k);
  }

  {
    vec_t
      v0 = vecij(B, nx - 1, ny - 1),
      vb = vecij(B, nx - 1, ny - 2),
      vl = vecij(B, nx - 2, ny - 1);;

    vec_t
      u0 = vunit(v0),
      ub = vunit(vb),
      ul = vunit(vl);

    double
      dudx = nandif(ul.x, u0.x, NAN) / dx,
      dudy = nandif(ub.x, u0.x, NAN) / dy,
      dvdx = nandif(ul.y, u0.y, NAN) / dx,
      dvdy = nandif(ub.y, u0.y, NAN) / dy;

    mat_t M = {dudx, dudy, dvdx, dvdy};
    vec_t vk = mvmul(M, u0);
    double k = (bend2v(u0, vk) == rightward ? 1 : -1) * vabs(vk);

    bilinear_z_set(Bc, nx - 1, ny -1, &k);
  }

  return Bc;
}

/*
  integrate bilinear over the passed domain, we test whether a grid
  square is to be included in the integrand by checking whether its
  centre is inside the domain, we don't do anything fancy like trying
  to intersect the grid-square with the domain and using Green's theorem
  (that remains an option).  So there are plenty of edge-cases when this
  will be inaccurate.  But we don't need that much accuracy here, it's
  only to estimate the the ellipse density in calculating the initial fill.
*/

static inline double defint(double a, double b, double c, double d,
                            double X, double Y)
{
  double
    X2 = X / 2,
    Y2 = Y / 2;

  return X * Y * ( (a * (1 - X2) + b * X2) * (1 - Y2) +
                   (c * (1 - X2) + d * X2) * Y2 );
}

int bilinear_integrate(const bilinear_t *B, const domain_t *dom, double *I)
{
  size_t
    nx = bilinear_nx_get(B),
    ny = bilinear_ny_get(B),
    nz = bilinear_nz_get(B);
  bbox_t
    bbox = bilinear_bbox_get(B);
  double
    dx = bilinear_dx(B),
    dy = bilinear_dy(B),
    sum[nz];

  for (size_t k = 0 ; k < nz ; k++)
    sum[k] = 0;

  for (size_t i = 0 ; i < nx - 1 ; i++)
    {
      double x = i * dx + bbox.x.min;

      for (size_t j = 0 ; j < ny - 1 ; j++)
        {
          double y = j * dy + bbox.y.min;
          vec_t midpoint = {x + dx / 2, y + dy / 2};

          if (! domain_inside(dom, midpoint))
            continue;

          double z00[nz], z01[nz], z10[nz], z11[nz];

          zij(B, i, j, z00);
          zij(B, i, j + 1, z01);
          zij(B, i + 1, j, z10);
          zij(B, i + 1, j + 1, z11);

          for (size_t k = 0 ; k < nz ; k++)
            {
              double
                z00k = z00[k],
                z01k = z01[k],
                z10k = z10[k],
                z11k = z11[k];

              if (isnan(z00k) || isnan(z01k) || isnan(z10k) || isnan(z11k))
                continue;

              sum[k] +=
                defint(z00k, z10k, z01k, z11k, 1, 1) -
                defint(z00k, z10k, z01k, z11k, 0, 1) -
                defint(z00k, z10k, z01k, z11k, 1, 0) +
                defint(z00k, z10k, z01k, z11k, 0, 0);
            }
        }
    }

  for (size_t k = 0 ; k < nz ; k++)
    I[k] = sum[k] * dx * dy;

  return 0;
}

/*
  return a grid to be used in bilinear pixel-domain, it is a
  node-aligned grid (it has to be, bilinear grids are naturally
  node aligned), but with the same boundary as a pixel-aligned
  grid corresponding to the input, hence "pixel proxy"
*/

static bilinear_t* pixel_proxy(const bilinear_t *B)
{
  size_t
    nx = bilinear_nx_get(B),
    ny = bilinear_ny_get(B),
    nz = bilinear_nz_get(B);

  bilinear_t *P;

  if ((P = bilinear_new(nx + 1, ny + 1, 1)) != NULL)
    {
      double
        dx = bilinear_dx(B),
        dy = bilinear_dy(B);

      bbox_t
        bB = bilinear_bbox_get(B),
        bP = {
          .x = {
            .min = bB.x.min - dx / 2,
            .max = bB.x.max + dx / 2,
          },
          .y = {
            .min = bB.y.min - dy / 2,
            .max = bB.y.max + dy / 2
          }
        };

      bilinear_bbox_set(P, bP);

      for (size_t i = 0 ; i < nx ; i++)
        {
          for (size_t j = 0 ; j < ny ; j++)
            {
              double z[nz];

              bilinear_z_get(B, i, j, z);

              if (! anynan(nz, z))
                {
                  double z0 = 1;

                  bilinear_z_set(P, i, j, &z0);
                  bilinear_z_set(P, i + 1, j, &z0);
                  bilinear_z_set(P, i, j + 1, &z0);
                  bilinear_z_set(P, i + 1, j + 1, &z0);
                }
            }
        }

      return P;
    }

  return NULL;
}

/* bilinear wrapper functions for domain-build */

static size_t domain_build_nx(const void *vB)
{
  const bilinear_t *B = vB;
  return bilinear_nx_get(B);
}

static size_t domain_build_ny(const void *vB)
{
  const bilinear_t *B = vB;
  return bilinear_ny_get(B);
}

static bool domain_build_present(const void *vB, size_t i, size_t j)
{
  const bilinear_t *B = vB;
  size_t nz = bilinear_nz_get(B);
  double z[nz];

  bilinear_z_get(B, i, j, z);

  return ! anynan(nz, z);
}

static void domain_build_xy(const void *vB, size_t i, size_t j, double xy[2])
{
  const bilinear_t *B = vB;

  bilinear_xy_get(B, i, j, xy);
}

domain_t* bilinear_node_domain(const bilinear_t *B)
{
  return
    domain_build(domain_build_nx,
                 domain_build_ny,
                 domain_build_present,
                 domain_build_xy,
                 B);
}

domain_t* bilinear_pixel_domain(const bilinear_t *B)
{
  bilinear_t *P;

  if ((P = pixel_proxy(B)) != NULL)
    {
      domain_t *D = bilinear_node_domain(P);
      bilinear_destroy(P);
      return D;
    }

  return NULL;
}
