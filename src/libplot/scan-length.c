#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/scan-length.h"
#include "plot/error.h"
#include "plot/units.h"

#include <log.h>

#include <stdio.h>

int scan_length(const char *p, const char *name, double *x)
{
  char c;
  double u;

  switch (sscanf(p, "%lf%c", x, &c))
    {
    case 0:
      log_error("%s option missing an argument", name);
      return ERROR_USER;
    case 1:
      c = 'p';
      /* fallthrough */
    case 2:
      if ((u = unit_ppt(c)) <= 0)
        {
          log_error("unknown unit %c in %s %s", c, name, p);
          unit_list_stream(stderr);
          return ERROR_USER;
        }
      break;
    default:
      return ERROR_BUG;
    }

  *x *= u;

  return ERROR_OK;
}
