#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/ellipse-mt.h"

#include "plot/anynan.h"
#include "plot/arrow.h"
#include "plot/evaluate.h"
#include "plot/error.h"

#include <geom2d/ellipse.h>
#include <geom2d/vec.h>

#include <math.h>

ellipse_mt_t* ellipse_mt_new(const ellipse_base_t *base)
{
  size_t
    nx = ellipse_base_nx(base),
    ny = ellipse_base_ny(base);
  ellipse_mt_t *mt;

  if ((mt = bilinear_new(nx, ny, 3)) == NULL)
    return NULL;

  bbox_t bbox = ellipse_base_bbox(base);

  bilinear_bbox_set(mt, bbox);

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
	{
          ellipse_t E;
	  int err = ellipse_base_get(base, i, j, &E);

	  switch (err)
	    {
	      mat_t M;

	    case ERROR_OK:

	      ellipse_mat(&E, &M);
              double z[3] = { M.a, M.b, M.d };
              bilinear_z_set(mt, i, j, z);
              break;

            case ERROR_NODATA:
              break;

            default:
              bilinear_destroy(mt);
              return NULL;
            }
        }
    }

  return mt;
}

void ellipse_mt_destroy(ellipse_mt_t *mt)
{
  bilinear_destroy(mt);
}

int ellipse_mt_eval(const ellipse_mt_t *mt, vec_t v, mat_t *M)
{
  double z[3];

  if (bilinear_eval(mt, v.x, v.y, z) != 0)
    return ERROR_BUG;

  if (anynan(3, z))
    return ERROR_NODATA;

  *M = (mat_t) { .a = z[0], .b = z[1], .c = z[1], .d = z[2] };

  return ERROR_OK;
}

int ellipse_mt_mat_get(const ellipse_mt_t *mt, size_t i, size_t j, mat_t *M)
{
  double z[3];

  bilinear_z_get(mt, i, j, z);

  if (anynan(3, z))
    return ERROR_NODATA;

  *M = (mat_t) { .a = z[0], .b = z[1], .c = z[1], .d = z[2] };

  return ERROR_OK;
}

size_t ellipse_mt_nx(const ellipse_mt_t *mt)
{
  return bilinear_nx_get(mt);
}

size_t ellipse_mt_ny(const ellipse_mt_t *mt)
{
  return bilinear_ny_get(mt);
}

bbox_t ellipse_mt_bbox(const ellipse_mt_t *mt)
{
  return bilinear_bbox_get(mt);
}
