#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/ellipse-density.h"

#include "plot/error.h"
#include "plot/bbox.h"

#include <geom2d/ellipse.h>
#include <geom2d/vec.h>

#include <math.h>

ellipse_density_t* ellipse_density_new(const ellipse_base_t *base)
{
  size_t
    nx = ellipse_base_nx(base),
    ny = ellipse_base_ny(base);
  ellipse_density_t *density;

  if ((density = bilinear_new(nx, ny, 1)) == NULL)
    return NULL;

  bbox_t bbox = ellipse_base_bbox(base);

  bilinear_bbox_set(density, bbox);

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
	{
          ellipse_t E;
          int err = ellipse_base_get(base, i, j, &E);

	  switch (err)
	    {
              double z;

	    case ERROR_OK:
              z = 1 / (E.major * E.minor * M_PI);
              bilinear_z_set(density, i, j, &z);
              break;

            case ERROR_NODATA:
              break;

            default:
              bilinear_destroy(density);
              return NULL;
            }
        }
    }

  return density;
}

int ellipse_density_integrate(const ellipse_density_t *density,
                              const domain_t *dom,
                              double *I)
{
  double Id[1];
  int err;

  if ((err = bilinear_integrate(density, dom, Id)) == 0)
    *I = Id[0];

  return err;
}

void ellipse_density_destroy(ellipse_density_t *density)
{
  bilinear_destroy(density);
}
