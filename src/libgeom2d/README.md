libgeom2d
---------

This is a (largely) standalone library implementing the Perram-Werthiem
[contact function][1] for 2-D ellipses (in `contact.c`) and its extension
to the semi-degenerate case (in `sdcontact.c`).  If you're interested in
extracting this from **vfplot** and incorporating it into your own project,
the only work which need to be done is to either remove the logging
functions in the code, or (easier) to provide a replacement `liblog`
whose `log.h` defines stub implementations:

``` c
#define log_error(...)
#define log_info(...)
```
[1]: https://doi.org/10.1016/0021-9991(85)90171-8
