#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <complex.h>
#include <stddef.h>

#include "geom2d/quadratic.h"
#include "geom2d/product-diff.h"

/*
  For a proper quadratic, find the complex double roots, returns
  non-zero on error
*/

int quadratic_roots_complex(const double a[static 3],
                            double complex r[static 2])
{
  double B, C, D;

  if (a[2] == 0)
    return 1;

  B = a[1] / a[2];
  C = a[0] / a[2];

  D = product_diff(B, B, 4, C);

  if (D > 0)
    {
      double sD = sqrt(D);

      if (B == 0)
        {
          r[0] = sD / 2;
          r[1] = -sD / 2;
        }
      else
        {
          double t = -(B + copysign(1, B) * sD) / 2;

          r[0] = t;
          r[1] = C / t;
        }
    }
  else if (D < 0)
    {
      double sD = sqrt(-D), R = -B / 2;

      r[0] = R + I * sD / 2;
      r[1] = R - I * sD / 2;
    }
  else
    {
      r[0] = r[1] = -B / 2;
    }

  return 0;
}
