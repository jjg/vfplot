#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>

#include "geom2d/mat.h"
#include "geom2d/sincos.h"
#include "geom2d/product-diff.h"

/* quadratic form (x, Mx) */

double mqform(mat_t M, vec_t x)
{
  return vdot(x, mvmul(M, x));
}

/* derivative of the same (M + M^T)x */

vec_t mdqform(mat_t M, vec_t x)
{
  return mvmul(madd(M, mtranspose(M)), x);
}

/* "principal direction", above with magnitude 1 */

vec_t mpdir(mat_t M, vec_t x)
{
  return vunit(mdqform(M, x));
}

/* the resolvent M - lambda I */

mat_t mres(mat_t M, double lambda)
{
  return (mat_t){
    M.a - lambda, M.b,
    M.c, M.d - lambda
  };
}

mat_t mrot(double t)
{
  double st, ct;
  sincos(t, &st, &ct);
  return (mat_t){
    ct, -st,
    st, ct
  };
}

mat_t mtranspose(mat_t M)
{
  return (mat_t){
    M.a, M.c,
    M.b, M.d
  };
}

mat_t madd(mat_t M, mat_t N)
{
  return (mat_t){
    M.a + N.a,  M.b + N.b,
    M.c + N.c,  M.d + N.d
  };
}

mat_t msub(mat_t M, mat_t N)
{
  return (mat_t){
    M.a - N.a, M.b - N.b,
    M.c - N.c, M.d - N.d
  };
}

mat_t msmul(double t, mat_t M)
{
  return (mat_t){
    t * M.a, t * M.b,
    t * M.c, t * M.d
  };
}

mat_t madj(mat_t M)
{
  return (mat_t){
    M.d, -M.b,
    -M.c, M.a
  };
}

mat_t minv(mat_t M)
{
  return msmul(1 / mdet(M), madj(M));
}

double mdet(mat_t M)
{
  return product_diff(M.a, M.d, M.b, M.c);
}

double mtrace(mat_t M)
{
  return M.a + M.d;
}

vec_t mvmul(mat_t M, vec_t u)
{
  return (vec_t){
    M.a * u.x + M.b * u.y,
    M.c * u.x + M.d * u.y
  };
}

mat_t mmmul(mat_t M, mat_t N)
{
  return (mat_t){
    M.a * N.a + M.b * N.c, M.a * N.b + M.b * N.d,
    M.c * N.a + M.d * N.c, M.c * N.b + M.d * N.d
  };
}

/*
  the maximum x-coordinate of the image of the unit circle
  transformed by this matrix -- with the circle paramaterised
  as (cos t, sin t) the image under M is

    f(t) = a cos(t) + b sin(t),

  differentiate, set to zero to find t (easier with atan2), then
  evaluste f at that value of t.  Likewise with ymax.

  This is used to get the bounding box of an ellipse in matrix
  representation, but note that the matrix is the rotation of the
  diagonal matrix of the square of the semi-axes, so one needs
  to take the square root of this ...
*/

double mxmax(mat_t M)
{
  double t = atan2(M.b, M.a), st, ct;
  sincos(t, &st, &ct);
  return fabs(M.a * ct + M.b * st);
}

double mymax(mat_t M)
{
  double t = atan2(M.d, M.c), st, ct;
  sincos(t, &st, &ct);
  return fabs(M.c * ct + M.d * st);
}
