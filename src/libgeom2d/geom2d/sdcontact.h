#ifndef GEOM2D_SDCONTACT_H
#define GEOM2D_SDCONTACT_H

#include <geom2d/mat.h>
#include <geom2d/vec.h>
#include <geom2d/ellipse.h>

int sdcontact(const ellipse_t*, const ellipse_t*, double*, vec_t*);
int sdcontact_mt(vec_t, mat_t, mat_t, double*, vec_t*);

#endif
