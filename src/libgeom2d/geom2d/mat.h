#ifndef GEOM2D_MAT_H
#define GEOM2D_MAT_H

#include <geom2d/vec.h>

typedef struct
{
  double a, b, c, d;
} mat_t;

mat_t mrot(double);

mat_t madd(mat_t, mat_t);
mat_t msub(mat_t, mat_t);
mat_t mtranspose(mat_t);

double mdet(mat_t);
mat_t minv(mat_t);
mat_t madj(mat_t);
double mtrace(mat_t);

mat_t msmul(double, mat_t);
vec_t mvmul(mat_t, vec_t);
mat_t mmmul(mat_t, mat_t);

mat_t mres(mat_t, double);

double mqform(mat_t, vec_t);
vec_t mdqform(mat_t, vec_t);
vec_t mpdir(mat_t, vec_t);

double mxmax(mat_t);
double mymax(mat_t);

#endif
