#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>

#include "geom2d/product-diff.h"

/*
  Calculate ab - cd, using fused multiply-add if available.

  Claude-Pierre Jeannerod, Nicolas Louvet, Jean-Michel Muller,
  "Further analysis of Kahan's algorithm for the accurate
  computation of 2 x 2 determinants." Mathematics of Computation,
  Vol. 82, No. 284, Oct. 2013, pp. 2245-2264
*/

#ifdef HAVE_FMA

double product_diff(double a, double b, double c, double d)
{
  double
    t = c * d,
    e = fma(c, d, -t),
    f = fma(a, b, -t);

  return f - e;
}

#else

double product_diff(double a, double b, double c, double d)
{
  return a * b - c * d;
}

#endif
