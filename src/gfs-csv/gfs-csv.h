/*
  gfs-csv.h

  read gfs (gerris) simulation data and write CSV

  J.J.Green 2007, 2022
*/

#ifndef GFS_CSV_H
#define GFS_CSV_H

#include <stdbool.h>
#include <stddef.h>

typedef struct
{
  bool index, verbose;
  char *delimiter;
  struct { size_t n; char **v; } scalar;
  struct { char *in, *out; } file;
} gfs_csv_t;

int gfs_csv(gfs_csv_t*);

#endif
