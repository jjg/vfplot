#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gfs-csv.h"

#include <log.h>

#include <gfs.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define G_DISABLE_ASSERT 1

typedef struct {
  struct {
    double min, max;
  } x, y;
} bbox_t;

static bbox_t bbox_join(bbox_t a, bbox_t b)
{
  return
    (bbox_t)
      {
        { fmin(a.x.min, b.x.min), fmax(a.x.max, b.x.max) },
        { fmin(a.y.min, b.y.min), fmax(a.y.max, b.y.max) }
      };
}

static double bbox_width(bbox_t b)
{
  return b.x.max - b.x.min;
}

static double bbox_height(bbox_t b)
{
  return b.y.max - b.y.min;
}

/*
  There is a more-or-less identical function in (ftt_cell_bbox)
  in the ftt.c of libgfs, but using a GtsBbox_t rather than our
  bbox_t, but with 1.99999 in place of 2 (I guess to avoid geometric
  degeneracy)
*/

static void ftt_bbox(FttCell *cell, gpointer data)
{
  FttVector p;
  ftt_cell_pos(cell, &p);

  double size = ftt_cell_size(cell) / 2;
  bbox_t
    bb = {{p.x - size, p.x + size}, {p.y - size, p.y + size}},
    *pbb = *(bbox_t**)data;

  if (pbb != NULL)
    *pbb = bbox_join(bb, *pbb);
  else
    {
      if ((pbb = malloc(sizeof(bbox_t))) != NULL)
        {
          *pbb = bb;
          *(bbox_t**)data = pbb;
        }
    }
}

/*
  This is a bit tricky - we want the i, j location of the centre point
  so that we can call the bilinear setz() functions, and one could
  probably do this cleverly by tracking the FTT_CELL_ID() of the cells
  as we traverse the tree. Here we hack it instead and calculate the
  (integer) i,js from the (double) x, y values of the centrepoint.

  the ffts_t structure is the data used by ftt_sample()
*/

typedef struct
{
  bool update;
  GfsFunction *f;
  GfsVariable *var;
} ftts_fun_t;

typedef struct
{
  bool index;
  const char *delimiter;
  int depth;
  bbox_t bb;
  FILE *sto;
  struct { size_t n; ftts_fun_t *v; } fun;
  struct { int cell, val, block; } stat;
} ftts_t;

static void ftt_sample(FttCell *cell, gpointer data)
{
  ftts_t *ftts = (ftts_t*)data;
  int level = ftt_cell_level(cell);
  double size = ftt_cell_size(cell);
  const char *delim = ftts->delimiter;

  FttVector p;
  ftt_cell_pos(cell, &p);

  /* the number in each direction we will sample */

  size_t n = pow(2, ftts->depth - level);

  /* coordinates at this box */

  int
    ic = floor((p.x - ftts->bb.x.min) / size),
    jc = floor((p.y - ftts->bb.y.min) / size);

  /* sample grid */

  double
    xmin = p.x - size / 2,
    ymin = p.y - size / 2,
    d = size / n;

  for (size_t i = 0 ; i < n ; i++)
    {
      double x = xmin + (i + 0.5) * d;
      int ig = ic * n + i;

      for (size_t j = 0 ; j < n ; j++)
        {
          double y = ymin + (j + 0.5) * d;
          int jg = jc * n + j;
          FttVector q = { .x = x, .y = y };

          if (ftts->index)
            fprintf(ftts->sto, "%i%s%i", ig, delim, jg);
          else
            fprintf(ftts->sto, "%.8f%s%.8f", x, delim, y);

          for (size_t k = 0 ; k < ftts->fun.n ; k++)
            {
              double z = gfs_interpolate(cell, q, ftts->fun.v[k].var);
              fprintf(ftts->sto, "%s%.16e", delim, z);
            }

          fprintf(ftts->sto, "\n");

          ftts->stat.val++;
        }
    }

  ftts->stat.block++;
}

static int gfs_csv_sti(FILE*, gfs_csv_t*);

int gfs_csv(gfs_csv_t *opt)
{
  int err = 0;
  gfs_init(&err, NULL);

  if (opt->file.in != NULL)
    {
      FILE *st;

      if ((st = fopen(opt->file.in, "r")) == NULL)
        {
          log_error("failed to open %s", opt->file.in);
          return 1;
        }

      err = gfs_csv_sti(st, opt);

      fclose(st);
    }
  else
    err = gfs_csv_sti(stdin, opt);

  return err;
}

static int gfs_csv_stio(FILE*, FILE*, gfs_csv_t*);

static int gfs_csv_sti(FILE *sti, gfs_csv_t *opt)
{
  int err = 0;

  if (opt->file.out != NULL)
    {
      FILE *sto;

      if ((sto = fopen(opt->file.out, "w")) == NULL)
        {
          log_error("failed to open %s", opt->file.out);
          return 1;
        }

      err = gfs_csv_stio(sti, sto, opt);

      fclose(sto);

      /*
        this use of unlink could be removed if we exchange opening
        output the input streams (so error on opening input meand
        we don't attempt to open output
      */

      if (err != 0)
        unlink(opt->file.out);
    }
  else
    err = gfs_csv_stio(sti, stdout, opt);

  return err;
}

static void update_var(FttCell *cell, gpointer data)
{
  ftts_t *ftts = (ftts_t*)data;
  ftts->stat.cell++;
  for (size_t i = 0 ; i < ftts->fun.n ; i++)
    {
      if (ftts->fun.v[i].update)
        GFS_VALUE(cell, ftts->fun.v[i].var) =
          gfs_function_value(ftts->fun.v[i].f, cell);
    }
}

static int gfs_csv_stio(FILE *sti, FILE *sto, gfs_csv_t *opt)
{
  GtsFile *flp = gts_file_new(sti);
  GfsSimulation *sim = gfs_simulation_read(flp);

  if (sim == NULL)
    {
      log_error("%s is not a valid GFS simulation",
                (opt->file.in ? opt->file.in : "input"));
      log_error("line %d:%d: %s", flp->line, flp->pos, flp->error);
      return 1;
    }

  GfsDomain *gdom = GFS_DOMAIN(sim);

  /* find the bounding box */

  bbox_t *bb = NULL;

  gfs_domain_cell_traverse(gdom,
                           FTT_PRE_ORDER,
                           FTT_TRAVERSE_ALL,
                           0,
                           ftt_bbox,
                           &bb);

  if (bb == NULL)
    {
      log_error("failed to determine bounding box");
      return 1;
    }

  log_info("bounding box %.1f/%.1f/%.1f/%.1f",
           bb->x.min,
           bb->x.max,
           bb->y.min,
           bb->y.max);

  /* tree depth and discretisation size */

  int
    depth = gfs_domain_depth(gdom),
    nw = round(pow(2, depth) * bbox_width(*bb)),
    nh = round(pow(2, depth) * bbox_height(*bb));

  log_info("grid is %i x %i", nw, nh);

  /* scalars */

  log_info("scalars:");
  for (size_t i = 0 ; i < opt->scalar.n ; i++)
    log_info("- %s", opt->scalar.v[i]);

  /* traverse to evaluate variable */

  ftts_t ftts;

  ftts.bb = *bb;
  ftts.depth = depth;
  ftts.index = opt->index;
  ftts.delimiter = opt->delimiter,
  ftts.sto = sto;
  ftts.stat.cell = 0;

  ftts_fun_t fun[opt->scalar.n];

  for (size_t i = 0 ; i < opt->scalar.n ; i++)
    {
      /* convert the function argument to a GfsFunction */

      GtsFile *fnp = gts_file_new_from_string(opt->scalar.v[i]);
      GfsFunction *f = gfs_function_new(gfs_function_class(), 0.0);

      /*
        having a nonsense string as opt->scalar.v[i] does not trip
        a sensible error here ...
      */

      gfs_function_read(f, gdom, fnp);

      if (fnp->type == GTS_ERROR)
        {
          log_error("bad function argument");
          log_error("%d: %s", fnp->pos, fnp->error);
          gts_file_destroy(fnp);
          return 1;
        }

      /* ... but does here ... */

      gfs_pending_functions_compilation(fnp);

      if (fnp->type == GTS_ERROR)
        {
          log_error("failed compile:");
          log_error("%d: %s", fnp->pos, fnp->error);
          gts_file_destroy(fnp);
          return 1;
        }

      gts_file_destroy(fnp);

      fun[i].f = f;

      if ((fun[i].var = gfs_function_get_variable(f)) == NULL)
        {
          fun[i].update = true;
          fun[i].var = gfs_temporary_variable(gdom);
        }
      else
        fun[i].update = false;
    }

  ftts.fun.n = opt->scalar.n;
  ftts.fun.v = fun;

  gfs_domain_cell_traverse(gdom,
                           FTT_PRE_ORDER,
                           FTT_TRAVERSE_LEAFS,
                           -1,
                           update_var,
                           &ftts);

  gts_file_destroy(flp);

  log_info("evaluated at %i cells%s",
           ftts.stat.cell,
           (ftts.stat.cell ? "" : " (internal)"));

  /* traverse to sample root cells */

  ftts.stat.val = 0;
  ftts.stat.block = 0;

  gfs_domain_cell_traverse(gdom,
                           FTT_PRE_ORDER,
                           FTT_TRAVERSE_LEAFS,
                           -1,
			   ftt_sample,
			   &ftts);

  if (ftts.stat.block > 0)
    log_info("wrote %i values (%.2f%%), inflated %.2f",
             ftts.stat.val,
             (double)(ftts.stat.val * 100) / (nw * nh),
             (double)(ftts.stat.val) / ftts.stat.block);
  else
    log_info("wrote %i values (%.2f%%)",
             ftts.stat.val,
             (double)(ftts.stat.val * 100) / (nw * nh));

  for (size_t i = 0 ; i < ftts.fun.n ; i++)
    gts_object_destroy(GTS_OBJECT(ftts.fun.v[i].f));

  /* clean up */

  free(bb);
  gts_object_destroy(GTS_OBJECT(sim));

  return 0;
}
