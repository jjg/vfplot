# to be included by Makefiles in subdirectories, so relative
# paths are ralative to those

SRC := ../..
VFPLOT := $(SRC)/vfplot/vfplot
GFSCSV := $(SRC)/gfs-csv/gfs-csv

FIXTURES := ../fixtures
