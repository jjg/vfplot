#!/usr/bin/env bash

version=$(cat ../../../VERSION)
fixture_dir='../fixtures'
src_dir='../..'

source 'config.bash'

function skip_unless_with_json
{
    if [ -z "$WITH_JSON" ] ; then
        skip 'no JSON'
    fi
}

function skip_unless_with_gerris
{
    if [ -z "$WITH_GERRIS" ] ; then
        skip 'no Gerris'
    fi
}

function skip_unless_with_netcdf
{
    if [ -z "$WITH_NETCDF" ] ; then
        skip 'no NetCDF'
    fi
}

function skip_unless_with_matlab
{
    if [ -z "$WITH_MATLAB" ] ; then
        skip 'no Matlab'
    fi
}
