#!/bin/sh

set -e

SIZE='5i'
BASE='pixel-aligned'
RNG='-R-1/1/-1/1'
PRJ="-JX${SIZE}"
EPS="${BASE}.eps"

gmt psbasemap $RNG $PRJ -B0.25 -K > $EPS
gmt psxy grid.dat $RNG $PRJ -Ss7p -G220 -K -O >> $EPS
gmt psxy grid.dat $RNG $PRJ -Sc1p -G0 -K -O >> $EPS
gmt psimage arrows.eps $RNG $PRJ -DjBL+w${SIZE} -K -O >> $EPS
gmt psxy escaped.dat $RNG $PRJ -Sc3p -Wthin,red -O >> $EPS

gmt psconvert -P -Tf -A $EPS
rm $EPS
