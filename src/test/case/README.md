Case tests
----------

This is a bit of an odd one, it runs `vfplot` against some fixtures
and creates a file; nothing is asserted (although it will fail if
the `vfplot` command fails).

Everything is done in the Makefile, and it is expected that would
only every be one target on which the `test`target  depends, but
earlier ones should be kept hanging around, one never knows when
one will want to use it again.
