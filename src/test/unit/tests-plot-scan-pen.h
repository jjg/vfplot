#include <CUnit/CUnit.h>

extern CU_TestInfo tests_scan_pen[];

void test_scan_pen_empty(void);
void test_scan_pen_width_only(void);
void test_scan_pen_colour(void);
