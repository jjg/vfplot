#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plot/scan-pen.h>
#include <plot/error.h>

#include "tests-plot-scan-pen.h"

CU_TestInfo tests_scan_pen[] = {
  { "empty", test_scan_pen_empty },
  { "width-only", test_scan_pen_width_only },
  { "colour", test_scan_pen_colour },
  CU_TEST_INFO_NULL
};

void test_scan_pen_empty(void)
{
  pen_t pen;

  CU_ASSERT_NOT_EQUAL(scan_pen("", &pen), ERROR_OK);
}

void test_scan_pen_width_only(void)
{
  pen_t pen;

  CU_ASSERT_EQUAL(scan_pen("1i", &pen), ERROR_OK);
  CU_ASSERT_DOUBLE_EQUAL(pen.width, 72, 1e-6);
  CU_ASSERT_EQUAL(pen.colour.type, colour_grey);
  CU_ASSERT_EQUAL(pen.colour.grey, 0);
}

void test_scan_pen_colour(void)
{
  pen_t pen;

  CU_ASSERT_EQUAL(scan_pen("1i/red", &pen), ERROR_OK);
  CU_ASSERT_DOUBLE_EQUAL(pen.width, 72, 1e-6);
  CU_ASSERT_EQUAL(pen.colour.type, colour_rgb);
  CU_ASSERT_EQUAL(pen.colour.rgb.r, 255);
  CU_ASSERT_EQUAL(pen.colour.rgb.g, 0);
  CU_ASSERT_EQUAL(pen.colour.rgb.b, 0);
}
