#include <CUnit/CUnit.h>

extern CU_TestInfo tests_field_read[];

void test_field_read_grd2_absent(void);
void test_field_read_grd2_cyl(void);
void test_field_read_grd2_small_node(void);
void test_field_read_grd2_small_pixel(void);
void test_field_read_grd2_small_mixed(void);
void test_field_read_gfs_absent(void);
void test_field_read_gfs_corrupt(void);
void test_field_read_gfs_house(void);
void test_field_read_mat_absent(void);
void test_field_read_mat_shear_v7(void);
void test_field_read_mat_shear_v7_pixel_false(void);
void test_field_read_mat_shear_v7_pixel_true(void);
void test_field_read_mat_shear_v7_range_absent(void);
void test_field_read_mat_shear_v7_range_sizes(void);
void test_field_read_mat_shear_v7_uv_absent(void);
void test_field_read_mat_shear_v7_uv_sizes(void);
void test_field_read_csv_absent(void);
void test_field_read_csv_simple_align_none(void);
void test_field_read_csv_simple_align_node(void);
void test_field_read_csv_simple_align_pixel(void);
void test_field_read_csv_simple_grid(void);
void test_field_read_csv_simple_range(void);
void test_field_read_csv_bad_line(void);
