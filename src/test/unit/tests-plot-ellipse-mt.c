#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plot/ellipse-mt.h>

#include "tests-plot-ellipse-mt.h"

CU_TestInfo tests_ellipse_mt[] = {
  {"construct", test_ellipse_mt_construct},
  {"nxy", test_ellipse_mt_nxy},
  {"bbox", test_ellipse_mt_bbox},
  CU_TEST_INFO_NULL
};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static int vfun(void *unused, double x, double y, double *u, double *v)
{
  *u = 1.0;
  *v = 0.0;
  return 0;
}

static int cfun(void *unused, double x, double y, double *crv)
{
  *crv = 0.0;
  return 0;
}

#pragma GCC diagnostic pop

static evaluate_t eval = {
  .fv = vfun,
  .fc = cfun,
  .field = NULL,
  .aspect = 3
};

static size_t nx = 10, ny = 10;
static bbox_t bbox = {{0, 10}, {0, 10}};
static arrow_opt_t opt = {
  .scale = 1,
  .bmaj = 0,
  .bmin = 0,
  .M = 1
};
static double eps = 1e-12;

void test_ellipse_mt_construct(void)
{
  ellipse_base_t *base = ellipse_base_new(bbox, nx, ny, &eval, &opt);
  CU_ASSERT_PTR_NOT_NULL_FATAL(base);
  ellipse_mt_t *mt = ellipse_mt_new(base);
  CU_ASSERT_PTR_NOT_NULL_FATAL(mt);
  ellipse_mt_destroy(mt);
  ellipse_mt_destroy(base);
}

void test_ellipse_mt_nxy(void)
{
  ellipse_base_t *base = ellipse_base_new(bbox, nx, ny, &eval, &opt);
  CU_ASSERT_PTR_NOT_NULL_FATAL(base);
  ellipse_mt_t *mt = ellipse_mt_new(base);
  CU_ASSERT_PTR_NOT_NULL_FATAL(mt);

  CU_ASSERT_EQUAL(nx, ellipse_mt_nx(mt));
  CU_ASSERT_EQUAL(ny, ellipse_mt_ny(mt));

  ellipse_mt_destroy(mt);
  ellipse_mt_destroy(base);
}

void test_ellipse_mt_bbox(void)
{
  ellipse_base_t *base = ellipse_base_new(bbox, nx, ny, &eval, &opt);
  CU_ASSERT_PTR_NOT_NULL_FATAL(base);

  ellipse_mt_t *mt = ellipse_mt_new(base);
  CU_ASSERT_PTR_NOT_NULL_FATAL(mt);

  bbox_t B = ellipse_mt_bbox(mt);

  CU_ASSERT_DOUBLE_EQUAL(bbox.x.min, B.x.min, eps);
  CU_ASSERT_DOUBLE_EQUAL(bbox.x.max, B.x.max, eps);
  CU_ASSERT_DOUBLE_EQUAL(bbox.y.min, B.y.min, eps);
  CU_ASSERT_DOUBLE_EQUAL(bbox.y.max, B.y.max, eps);

  ellipse_mt_destroy(mt);
  ellipse_mt_destroy(base);
}
