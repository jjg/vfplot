#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plot/scan-length.h>
#include <plot/error.h>

#include "tests-plot-scan-length.h"

CU_TestInfo tests_scan_length[] = {
  {"empty", test_scan_length_empty},
  {"no-unit", test_scan_length_no_unit},
  {"printer's point", test_scan_length_P},
  {"PostScript point", test_scan_length_p},
  {"inch", test_scan_length_i},
  {"millimetre", test_scan_length_m},
  {"centimetre", test_scan_length_c},
  CU_TEST_INFO_NULL
};

void test_scan_length_empty(void)
{
  double len;

  CU_ASSERT_NOT_EQUAL(scan_length("", "some-name", &len), ERROR_OK);
}

void test_scan_length_no_unit(void)
{
  double len;

  CU_ASSERT_EQUAL(scan_length("3", "some-name", &len), ERROR_OK);
  CU_ASSERT_DOUBLE_EQUAL(3, len, 1e-9);
}

void test_scan_length_P(void)
{
  double len;

  CU_ASSERT_EQUAL(scan_length("3P", "some-name", &len), ERROR_OK);
  CU_ASSERT_DOUBLE_EQUAL(2.98879203, len, 1e-9);
}

void test_scan_length_p(void)
{
  double len;

  CU_ASSERT_EQUAL(scan_length("3p", "some-name", &len), ERROR_OK);
  CU_ASSERT_DOUBLE_EQUAL(3, len, 1e-9);
}

void test_scan_length_i(void)
{
  double len;

  CU_ASSERT_EQUAL(scan_length("3i", "some-name", &len), ERROR_OK);
  CU_ASSERT_DOUBLE_EQUAL(216, len, 1e-9);
}

void test_scan_length_m(void)
{
  double len;

  CU_ASSERT_EQUAL(scan_length("3m", "some-name", &len), ERROR_OK);
  CU_ASSERT_DOUBLE_EQUAL(8.503937010, len, 1e-9);
}

void test_scan_length_c(void)
{
  double len;

  CU_ASSERT_EQUAL(scan_length("3c", "some-name", &len), ERROR_OK);
  CU_ASSERT_DOUBLE_EQUAL(85.03937010, len, 1e-9);
}
