#include <CUnit/CUnit.h>

extern CU_TestInfo tests_domain[];

void test_domain_new(void);
void test_domain_bbox_nested(void);
void test_domain_bbox_components(void);
void test_domain_orientate(void);
void test_domain_inside(void);
void test_domain_scale(void);
void test_domain_each(void);
void test_domain_insert(void);
void test_domain_clone(void);
void test_domain_area_square(void);
void test_domain_area_simple(void);
void test_domain_depth_empty(void);
void test_domain_depth_simple(void);
void test_domain_depth_square(void);
void test_domain_width_empty(void);
void test_domain_width_simple(void);
void test_domain_width_square(void);
