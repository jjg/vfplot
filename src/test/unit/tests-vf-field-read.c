#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <vf/field.h>

#include "tests-vf-field-read.h"
#include "fixture.h"
#include "assert-bbox.h"

CU_TestInfo tests_field_read[] = {
  {"grd2, absent", test_field_read_grd2_absent},
  {"grd2, cyl", test_field_read_grd2_cyl},
  {"grd2, small-node", test_field_read_grd2_small_node},
  {"grd2, small-pixel", test_field_read_grd2_small_pixel},
  {"grd2, small-mixed", test_field_read_grd2_small_mixed},
  {"gfs, absent", test_field_read_gfs_absent},
  {"gfs, corrupt", test_field_read_gfs_corrupt},
  {"gfs, house-001", test_field_read_gfs_house},
  {"mat, absent", test_field_read_mat_absent},
  {"mat, shear-v7-pixel-false", test_field_read_mat_shear_v7_pixel_false},
  {"mat, shear-v7-pixel-true", test_field_read_mat_shear_v7_pixel_true},
  {"mat, shear-v7-range-absent", test_field_read_mat_shear_v7_range_absent},
  {"mat, shear-v7-range-sizes", test_field_read_mat_shear_v7_range_sizes},
  {"mat, shear-v7-uv-absent", test_field_read_mat_shear_v7_uv_absent},
  {"mat, shear-v7-uv-sizes", test_field_read_mat_shear_v7_uv_sizes},
  {"csv, absent", test_field_read_csv_absent},
  {"csv, simple, align none", test_field_read_csv_simple_align_none},
  {"csv, simple, align node", test_field_read_csv_simple_align_node},
  {"csv, simple, align pixel", test_field_read_csv_simple_align_pixel},
  {"csv, simple, grid", test_field_read_csv_simple_grid},
  {"csv, simple, range", test_field_read_csv_simple_range},
  {"csv, bad line", test_field_read_csv_bad_line},
  CU_TEST_INFO_NULL
};


void test_field_read_grd2_absent(void)
{
  const char* paths[2] = {
    "tmp/absent/u.grd",
    "tmp/absent/v.grd"
  };

  field_t *field =
    field_read(format_grd2,
               align_none,
               2, paths,
               NULL, NULL);

#ifdef WITH_NETCDF
  CU_ASSERT_PTR_NULL(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif
}

void test_field_read_grd2_cyl(void)
{
  char *path[2] = {
    strdup(fixture("grd/cyl-u.grd")),
    strdup(fixture("grd/cyl-v.grd"))
  };

  CU_ASSERT_PTR_NOT_NULL_FATAL(path[0]);
  CU_ASSERT_PTR_NOT_NULL_FATAL(path[1]);

  field_align_t aligns[3] = {
    align_none,
    align_node,
    align_pixel
  };

  for (size_t i = 0 ; i < 3 ; i++)
    {
      field_t *field =
        field_read(format_grd2,
                   aligns[i],
                   2, (const char**)path,
                   NULL, NULL);

#ifdef WITH_NETCDF
      CU_ASSERT_PTR_NOT_NULL_FATAL(field);
      CU_ASSERT_EQUAL(field_align(field), align_pixel);
      field_destroy(field);
#else
      CU_ASSERT_PTR_NULL(field);
#endif
    }

  free(path[0]);
  free(path[1]);
}

void test_field_read_grd2_small_node(void)
{
  char *path[2] = {
    strdup(fixture("grd/small-node-u.grd")),
    strdup(fixture("grd/small-node-v.grd"))
  };

  CU_ASSERT_PTR_NOT_NULL_FATAL(path[0]);
  CU_ASSERT_PTR_NOT_NULL_FATAL(path[1]);

  field_t *field =
    field_read(format_none,
               align_none,
               2, (const char**)path,
               NULL, NULL);

#ifdef WITH_NETCDF
  CU_ASSERT_PTR_NOT_NULL_FATAL(field);
  CU_ASSERT_EQUAL(field_align(field), align_node);

  domain_t *dom = field_methods.domain(field);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  bbox_t bbox1, bbox2 = {
    .x = {.min = 0, .max = 2},
    .y = {.min = 0, .max = 2}
  };

  CU_ASSERT_EQUAL(domain_bbox(dom, &bbox1), 0);
  CU_ASSERT_BBOX_EQUAL(bbox1, bbox2, 1e-12);

  domain_destroy(dom);
  field_destroy(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif

  free(path[0]);
  free(path[1]);
}

void test_field_read_grd2_small_pixel(void)
{
  char *path[2] = {
    strdup(fixture("grd/small-pixel-u.grd")),
    strdup(fixture("grd/small-pixel-v.grd"))
  };

  CU_ASSERT_PTR_NOT_NULL_FATAL(path[0]);
  CU_ASSERT_PTR_NOT_NULL_FATAL(path[1]);

  field_t *field =
    field_read(format_none,
               align_none,
               2, (const char**)path,
               NULL, NULL);

#ifdef WITH_NETCDF
  CU_ASSERT_PTR_NOT_NULL_FATAL(field);
  CU_ASSERT_EQUAL(field_align(field), align_pixel);

  domain_t *dom = field_methods.domain(field);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  bbox_t bbox1, bbox2 = {
    .x = {.min = 0, .max = 2},
    .y = {.min = 0, .max = 2}
  };

  CU_ASSERT_EQUAL(domain_bbox(dom, &bbox1), 0);
  CU_ASSERT_BBOX_EQUAL(bbox1, bbox2, 1e-12);

  domain_destroy(dom);
  field_destroy(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif

  free(path[0]);
  free(path[1]);
}

void test_field_read_grd2_small_mixed(void)
{
  char *path[2] = {
    strdup(fixture("grd/small-node-u.grd")),
    strdup(fixture("grd/small-pixel-v.grd"))
  };

  CU_ASSERT_PTR_NOT_NULL_FATAL(path[0]);
  CU_ASSERT_PTR_NOT_NULL_FATAL(path[1]);

  field_t *field =
    field_read(format_none,
               align_none,
               2, (const char**)path,
               NULL, NULL);

#ifdef WITH_NETCDF
  CU_ASSERT_PTR_NULL(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif

  free(path[0]);
  free(path[1]);
}

void test_field_read_gfs_house(void)
{
  const char *path = fixture("gfs/house-001.gfs");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_none, align_none, 1, &path, NULL, NULL);

#ifdef WITH_GERRIS
  CU_ASSERT_PTR_NOT_NULL_FATAL(field);
  CU_ASSERT_EQUAL(field_align(field), align_pixel);
  field_destroy(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif
}

void test_field_read_gfs_absent(void)
{
  const char *path = "tmp/absent.gfs";

  field_t *field =
    field_read(format_gfs, align_none, 1, &path, NULL, NULL);

#ifdef WITH_GERRIS
  CU_ASSERT_PTR_NULL(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif
}

void test_field_read_gfs_corrupt(void)
{
  const char *path = fixture("gfs/corrupt.gfs");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_gfs, align_none, 1, &path, NULL, NULL);

#ifdef WITH_GERRIS
  CU_ASSERT_PTR_NULL(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif
}

void test_field_read_mat_absent(void)
{
  const char *path = "tmp/absent.mat";

  field_t *field =
    field_read(format_mat, align_none, 1, &path, NULL, NULL);

#ifdef WITH_MATLAB
  CU_ASSERT_PTR_NULL(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif
}

void test_field_read_mat_shear_v7(void)
{
  const char *path = fixture("mat/shear-v7.mat");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_none, align_none, 1, &path, NULL, NULL);

#ifdef WITH_MATLAB
  CU_ASSERT_PTR_NOT_NULL_FATAL(field);
  CU_ASSERT_EQUAL(field_align(field), align_node);
  field_destroy(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif
}

void test_field_read_mat_shear_v7_pixel_false(void)
{
  const char *path = fixture("mat/shear-v7-pixel-false.mat");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_none, align_none, 1, &path, NULL, NULL);

#ifdef WITH_MATLAB
  CU_ASSERT_PTR_NOT_NULL_FATAL(field);
  CU_ASSERT_EQUAL(field_align(field), align_node);

  domain_t *dom = field_methods.domain(field);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  bbox_t bbox1, bbox2 = {
    .x = {.min = 0, .max = 2},
    .y = {.min = 0, .max = 1}
  };

  CU_ASSERT_EQUAL(domain_bbox(dom, &bbox1), 0);
  CU_ASSERT_BBOX_EQUAL(bbox1, bbox2, 1e-12);

  domain_destroy(dom);
  field_destroy(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif
}

void test_field_read_mat_shear_v7_pixel_true(void)
{
  const char *path = fixture("mat/shear-v7-pixel-true.mat");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_none, align_none, 1, &path, NULL, NULL);

#ifdef WITH_MATLAB
  CU_ASSERT_PTR_NOT_NULL_FATAL(field);
  CU_ASSERT_EQUAL(field_align(field), align_pixel);

  domain_t *dom = field_methods.domain(field);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  bbox_t bbox1, bbox2 = {
    .x = {.min = 0, .max = 2},
    .y = {.min = 0, .max = 1}
  };

  CU_ASSERT_EQUAL(domain_bbox(dom, &bbox1), 0);
  CU_ASSERT_BBOX_EQUAL(bbox1, bbox2, 1e-12);

  domain_destroy(dom);
  field_destroy(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif
}

void test_field_read_mat_shear_v7_range_absent(void)
{
  const char *path = fixture("mat/shear-v7-range-absent.mat");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_none, align_none, 1, &path, NULL, NULL);

#ifdef WITH_MATLAB
  CU_ASSERT_PTR_NOT_NULL_FATAL(field);
  field_destroy(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif
}

void test_field_read_mat_shear_v7_range_sizes(void)
{
  const char *path = fixture("mat/shear-v7-range-sizes.mat");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_none, align_none, 1, &path, NULL, NULL);

#ifdef WITH_MATLAB
  CU_ASSERT_PTR_NULL(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif
}

void test_field_read_mat_shear_v7_uv_absent(void)
{
  const char *path = fixture("mat/shear-v7-uv-absent.mat");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_none, align_none, 1, &path, NULL, NULL);

#ifdef WITH_MATLAB
  CU_ASSERT_PTR_NULL(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif
}

void test_field_read_mat_shear_v7_uv_sizes(void)
{
  const char *path = fixture("mat/shear-v7-uv-sizes.mat");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_none, align_none, 1, &path, NULL, NULL);

#ifdef WITH_MATLAB
  CU_ASSERT_PTR_NULL(field);
#else
  CU_ASSERT_PTR_NULL(field);
#endif
}

void test_field_read_csv_absent(void)
{
  const char *path = "tmp/absent.csv";

  field_t *field =
    field_read(format_csv, align_none, 1, &path, NULL, NULL);

  CU_ASSERT_PTR_NULL(field);
}

void test_field_read_csv_simple_align_none(void)
{
  const char *path = fixture("csv/simple.csv");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_csv, align_none, 1, &path, NULL, NULL);

  CU_ASSERT_PTR_NOT_NULL_FATAL(field);
  CU_ASSERT_EQUAL(field_align(field), align_node);
  field_destroy(field);
}

void test_field_read_csv_simple_align_node(void)
{
  const char *path = fixture("csv/simple.csv");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_csv, align_node, 1, &path, NULL, NULL);

  CU_ASSERT_PTR_NOT_NULL_FATAL(field);
  CU_ASSERT_EQUAL(field_align(field), align_node);
  field_destroy(field);
}

void test_field_read_csv_simple_align_pixel(void)
{
  const char *path = fixture("csv/simple.csv");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_csv, align_pixel, 1, &path, NULL, NULL);

  CU_ASSERT_PTR_NOT_NULL_FATAL(field);
  CU_ASSERT_EQUAL(field_align(field), align_pixel);
  field_destroy(field);
}

void test_field_read_csv_bad_line(void)
{
  const char *path = fixture("csv/bad-line.csv");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_t *field =
    field_read(format_csv, align_none, 1, &path, NULL, NULL);

  CU_ASSERT_PTR_NULL(field);
}

void test_field_read_csv_simple_grid(void)
{
  const char *path = fixture("csv/simple.csv");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_grid_t grid = { .size = {3, 4} };

  field_t *field =
    field_read(format_csv, align_none, 1, &path, &grid, NULL);

  CU_ASSERT_PTR_NOT_NULL_FATAL(field);
  field_destroy(field);
}

void test_field_read_csv_simple_range(void)
{
  const char *path = fixture("csv/simple.csv");

  CU_ASSERT_PTR_NOT_NULL_FATAL(path);

  field_range_t range = {
    .x = { .min = 0, .max = 1 },
    .y = { .min = 0, .max = 1 }
  };

  field_t *field =
    field_read(format_csv, align_pixel, 1, &path, NULL, &range);

  CU_ASSERT_PTR_NOT_NULL_FATAL(field);

  /*
    if you have align_node (or align_none) in the above, then
    you end up with a domain which does have the same bounding
    box as the range, this is because 'simple.csv' is rather
    sparse, and isolated points are ignored in construction of
    the domain.
  */

  domain_t *dom = field_methods.domain(field);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  bbox_t bbox1, bbox2 = {
    .x = {.min = 0, .max = 1},
    .y = {.min = 0, .max = 1}
  };

  CU_ASSERT_EQUAL(domain_bbox(dom, &bbox1), 0);
  CU_ASSERT_BBOX_EQUAL(bbox1, bbox2, 1e-12);

  domain_destroy(dom);
  field_destroy(field);
}
