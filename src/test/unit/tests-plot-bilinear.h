#include <CUnit/CUnit.h>

extern CU_TestInfo tests_bilinear[];

void test_bilinear_new(void);
void test_bilinear_xy_get(void);
void test_bilinear_z_get_set(void);
void test_bilinear_csv_write(void);
void test_bilinear_eval_quadratic(void);
void test_bilinear_eval_nodata(void);
void test_bilinear_curvature_linear(void);
void test_bilinear_curvature_circular(void);
void test_bilinear_integrate_interior(void);
void test_bilinear_integrate_intersect(void);
void test_bilinear_node_domain_01(void);
void test_bilinear_node_domain_02(void);
void test_bilinear_node_domain_03(void);
void test_bilinear_pixel_domain_01(void);
void test_bilinear_pixel_domain_02(void);
void test_bilinear_pixel_domain_03(void);
