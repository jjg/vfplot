#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <CUnit/CUnit.h>

#include "tests-plot-arrow.h"
#include "tests-plot-aspect.h"
#include "tests-plot-bbox.h"
#include "tests-plot-bilinear.h"
#include "tests-plot-contain.h"
#include "tests-plot-csv-read.h"
#include "tests-plot-csv-write.h"
#include "tests-plot-curvature.h"
#include "tests-plot-domain.h"
#include "tests-plot-domain-geojson.h"
#include "tests-plot-ellipse-mt.h"
#include "tests-plot-gstate.h"
#include "tests-plot-margin.h"
#include "tests-plot-polygon.h"
#include "tests-plot-potential.h"
#include "tests-plot-svg-colours.h"
#include "tests-plot-scan-colour.h"
#include "tests-plot-scan-crop.h"
#include "tests-plot-scan-fill.h"
#include "tests-plot-scan-length.h"
#include "tests-plot-scan-pen.h"
#include "tests-plot-units.h"

#include "cunit-compat.h"

#define ENTRY(a, b) CU_Suite_Entry(a, NULL, NULL, (b))

static CU_SuiteInfo suites[] =
  {
    ENTRY("arrows", tests_arrow),
    ENTRY("aspect ratio", tests_aspect),
    ENTRY("bounding boxes", tests_bbox),
    ENTRY("bilinear", tests_bilinear),
    ENTRY("CSV read", tests_csv_read),
    ENTRY("CSV write", tests_csv_write),
    ENTRY("contain", tests_contain),
    ENTRY("curvature", tests_curvature),
    ENTRY("domain", tests_domain),
    ENTRY("domain-geojson", tests_domain_geojson),
    ENTRY("ellipse-mt", tests_ellipse_mt),
    ENTRY("gstate", tests_gstate),
    ENTRY("margin", tests_margin),
    ENTRY("polygon", tests_polygon),
    ENTRY("potential", tests_potential),
    ENTRY("scan-colour", tests_scan_colour),
    ENTRY("scan-crop", tests_scan_crop),
    ENTRY("scan-fill", tests_scan_fill),
    ENTRY("scan-length", tests_scan_length),
    ENTRY("scan-pen", tests_scan_pen),
    ENTRY("svg-colours", tests_svg_colours),
    ENTRY("units", tests_units),
    CU_SUITE_INFO_NULL,
  };

void suites_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr,
              "plot suite registration failed - %s\n",
	      CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}
