/*
  assert_domain.c
  J.J. Green 2023
*/

#include <math.h>
#include <stdlib.h>

#include <CUnit/CUnit.h>

#include <plot/bbox.h>
#include <geom2d/vec.h>

#include "assert-domain.h"

/*
  this assertion is "approximate" in the sense that it may
  pass when the arguments differ, but should never fail if
  the arguments are identical.  It generates a bunch of
  random vectors in the joined bounding box of the arguments,
  then checks that "domain_inside" gives the same result for
  each point in both domains.  So it does not inspect the
  interior of the domain (it does not know the interior),
  it acts purely on the behaviour of the domain.
*/

static double rand_range(double min, double max)
{
  double
    range = max - min,
    div = RAND_MAX / range;

  return min + (rand() / div);
}

static vec_t rand_vec(bbox_t bb)
{
  vec_t v = {
    .x = rand_range(bb.x.min, bb.x.max),
    .y = rand_range(bb.y.min, bb.y.max)
  };
  return v;
}

void assert_domain_equal(const domain_t *d1, const domain_t *d2, size_t N)
{
  bbox_t bb1, bb2, bb;

  CU_ASSERT_EQUAL_FATAL(domain_bbox(d1, &bb1), 0);
  CU_ASSERT_EQUAL_FATAL(domain_bbox(d2, &bb2), 0);

  bbox_join(&bb1, &bb2, &bb);

  for (size_t i = 0 ; i < N ; i++)
    {
      vec_t v = rand_vec(bb);
      CU_ASSERT_EQUAL_FATAL(domain_inside(d1, v), domain_inside(d2, v));
    }
}
