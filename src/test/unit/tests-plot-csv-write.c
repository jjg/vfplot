#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>

#include <plot/csv-write.h>

#include "tests-plot-csv-write.h"

CU_TestInfo tests_csv_write[] = {
  {"3x3 zero grid", test_csv_write_3x3_zero},
  CU_TEST_INFO_NULL
};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static int f0(void *unused, double x, double y, double *t, double *m)
{
  *t = 0;
  *m = 0;

  return 0;
}

#pragma GCC diagnostic pop

void test_csv_write_3x3_zero(void)
{
  bbox_t bbox = {{0, 3}, {0, 3}};
  polygon_t p;
  CU_ASSERT_EQUAL(polygon_rect(bbox, &p), 0);

  domain_t *dom = domain_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  CU_ASSERT_EQUAL(domain_insert(dom, &p), 0);

  const char path[] = "tmp/csv-write-3x3.txt";

  CU_ASSERT_EQUAL(csv_write(path, dom, f0, NULL, 3, 3), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);

  domain_destroy(dom);
  unlink(path);
}
