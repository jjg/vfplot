#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <CUnit/CUnit.h>

#include "tests-vf-field-range.h"
#include "tests-vf-field-read.h"

#include "cunit-compat.h"

#define ENTRY(a, b) CU_Suite_Entry(a, NULL, NULL, (b))

static CU_SuiteInfo suites[] = {
  ENTRY("field range", tests_field_range),
  ENTRY("field read", tests_field_read),
  CU_SUITE_INFO_NULL,
};

void suites_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr,
              "vf suite registration failed - %s\n",
	      CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}
