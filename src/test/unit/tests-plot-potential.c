#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plot/potential.h>
#include "tests-plot-potential.h"

CU_TestInfo tests_potential[] = {
  {"samples", test_potential_samples},
  CU_TEST_INFO_NULL,
};

void test_potential_samples(void)
{
  double eps = 1e-10;

  CU_ASSERT_DOUBLE_EQUAL(potential_V(1.0, 0.7), 0.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(potential_V(1.0, 1.0), 0.0, eps);

  CU_ASSERT_DOUBLE_EQUAL(potential_V(2.0, 0.7), 0.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(potential_V(2.0, 1.0), 0.0, eps);

  CU_ASSERT(potential_V(0.7, 0.7) > 0);
  CU_ASSERT(potential_V(0.0, 0.7) > 0);

  CU_ASSERT_DOUBLE_EQUAL(potential_dV(0.0, 0.7), -1.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(potential_dV(0.0, 1.0), -1.0, eps);

  CU_ASSERT_DOUBLE_EQUAL(potential_dV(0.7, 0.7), -1.0, eps);

  CU_ASSERT_DOUBLE_EQUAL(potential_dV(2.0, 0.7), 0.0, eps);
  CU_ASSERT_DOUBLE_EQUAL(potential_dV(2.0, 1.0), 0.0, eps);

  CU_ASSERT(potential_dV(0.9, 0.8) <  0.0);
  CU_ASSERT(potential_dV(0.9, 0.8) > -1.0);
}
