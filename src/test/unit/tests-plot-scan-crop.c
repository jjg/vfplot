#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plot/scan-crop.h>
#include <plot/error.h>

#include "tests-plot-scan-crop.h"

CU_TestInfo tests_scan_crop[] = {
  {"empty", test_scan_crop_empty},
  {"single", test_scan_crop_1},
  {"pair", test_scan_crop_2},
  {"triple", test_scan_crop_3},
  {"quad", test_scan_crop_4},
  CU_TEST_INFO_NULL
};

void test_scan_crop_empty(void)
{
  crop_t crop;

  CU_ASSERT_NOT_EQUAL(scan_crop("", &crop), ERROR_OK);
}

void test_scan_crop_1(void)
{
  crop_t crop;

  CU_ASSERT_EQUAL(scan_crop("1i", &crop), ERROR_OK);
  CU_ASSERT_DOUBLE_EQUAL(crop.x.min, 72, 1e-6);
  CU_ASSERT_DOUBLE_EQUAL(crop.x.max, 72, 1e-6);
  CU_ASSERT_DOUBLE_EQUAL(crop.y.min, 72, 1e-6);
  CU_ASSERT_DOUBLE_EQUAL(crop.y.max, 72, 1e-6);
}

void test_scan_crop_2(void)
{
  crop_t crop;

  CU_ASSERT_EQUAL(scan_crop("1i/2i", &crop), ERROR_OK);
  CU_ASSERT_DOUBLE_EQUAL(crop.x.min, 72, 1e-6);
  CU_ASSERT_DOUBLE_EQUAL(crop.x.max, 72, 1e-6);
  CU_ASSERT_DOUBLE_EQUAL(crop.y.min, 144, 1e-6);
  CU_ASSERT_DOUBLE_EQUAL(crop.y.max, 144, 1e-6);
}

void test_scan_crop_3(void)
{
  crop_t crop;

  CU_ASSERT_NOT_EQUAL(scan_crop("1i/2i/3i", &crop), ERROR_OK);
}

void test_scan_crop_4(void)
{
  crop_t crop;

  CU_ASSERT_EQUAL(scan_crop("1i/2i/1i/1i", &crop), ERROR_OK);
  CU_ASSERT_DOUBLE_EQUAL(crop.x.min, 72, 1e-6);
  CU_ASSERT_DOUBLE_EQUAL(crop.x.max, 144, 1e-6);
  CU_ASSERT_DOUBLE_EQUAL(crop.y.min, 72, 1e-6);
  CU_ASSERT_DOUBLE_EQUAL(crop.y.max, 72, 1e-6);
}
