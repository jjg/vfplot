#include <CUnit/CUnit.h>

extern CU_TestInfo tests_sdcontact[];

void test_sdcontact_circle_radial(void);
void test_sdcontact_circle_tangent_mid(void);
void test_sdcontact_circle_tangent_end(void);
void test_sdcontact_circle_tangent_ext(void);
void test_sdcontact_circle_skew_end(void);
void test_sdcontact_rotation_invariance(void);
void test_sdcontact_non_negativity(void);
void test_sdcontact_non_negativity_regress_1(void);
void test_sdcontact_non_negativity_regress_2(void);
void test_sdcontact_div_zero_regress(void);
