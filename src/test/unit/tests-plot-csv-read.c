#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <plot/csv-read.h>

#include "fixture.h"
#include "tests-plot-csv-read.h"

CU_TestInfo tests_csv_read[] = {
  {"valid fixture", test_csv_read_valid_fixture},
  {"no such file", test_csv_read_no_such_file},
  CU_TEST_INFO_NULL,
};

void test_csv_read_valid_fixture(void)
{
  csv_read_t S;
  const char *path = fixture("csv/simple.csv");

  CU_ASSERT_EQUAL_FATAL(csv_read_open(path, &S), CSV_READ_OK);

  int err;
  double v[2];
  size_t n[2], lines = 0;

  struct {
    size_t n[2];
    double v[2];
  }
  expected[]  =
    {
     { {0, 0}, {3.0, 3.0} },
     { {0, 1}, {1.2, 3.2} },
     { {1, 1}, {2.3, 3.2} },
     { {2, 1}, {1.7, 5.1} },
     { {2, 2}, {1.1, 3.2} },
     { {2, 3}, {0.1, 0.2} }
    };

  do {
    err = csv_read_line(&S, n, v);
    switch (err)
      {
      case CSV_READ_OK:
        CU_ASSERT_EQUAL(n[0], expected[lines].n[0]);
        CU_ASSERT_EQUAL(n[1], expected[lines].n[1]);
        CU_ASSERT_DOUBLE_EQUAL(v[0], expected[lines].v[0], 1e-12);
        CU_ASSERT_DOUBLE_EQUAL(v[1], expected[lines].v[1], 1e-12);
	lines++;
	break;
      case CSV_READ_NODATA:
      case CSV_READ_EOF:
	break;
      default:
	CU_FAIL_FATAL("error from csv_read_line");
      };
  }
  while (err != CSV_READ_EOF);

  CU_ASSERT_EQUAL(lines, 6);

  csv_read_close(&S);
}

void test_csv_read_no_such_file(void)
{
  csv_read_t S;
  const char *path = fixture("no-such-file.sag");
  CU_ASSERT_EQUAL_FATAL(csv_read_open(path, &S), CSV_READ_ERROR);
}
