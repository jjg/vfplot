/*
  assert_mat.h
  Copyright (c) J.J. Green 2015
*/

#ifndef ASSERT_MATRIX_H
#define ASSERT_MATRIX_H

#include <geom2d/mat.h>

#define CU_ASSERT_MAT_EQUAL(A, B, eps) assert_mat_equal(A, B, eps)

void assert_mat_equal(mat_t, mat_t, double);

#endif
