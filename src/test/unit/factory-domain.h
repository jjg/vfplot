/*
  factory_domain.h
  Copyright (c) J.J. Green 2022
*/

#ifndef FACTORY_DOMAIN_H
#define FACTORY_DOMAIN_H

#include <plot/domain.h>

domain_t* factory_domain_square(void);
domain_t* factory_domain_nested(void);
domain_t* factory_domain_components(void);

#endif
