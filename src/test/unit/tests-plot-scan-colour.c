#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plot/scan-colour.h>
#include <plot/error.h>

#include "tests-plot-scan-colour.h"

CU_TestInfo tests_scan_colour[] = {
  { "empty", test_scan_colour_empty },
  { "grey", test_scan_colour_grey },
  { "RGB", test_scan_colour_rgb },
  { "named", test_scan_colour_named },
  CU_TEST_INFO_NULL
};

void test_scan_colour_empty(void)
{
  colour_t col;

  CU_ASSERT_NOT_EQUAL(scan_colour("", &col), ERROR_OK);
}

void test_scan_colour_grey(void)
{
  colour_t col;

  CU_ASSERT_EQUAL(scan_colour("20", &col), ERROR_OK);
  CU_ASSERT_EQUAL(col.type, colour_grey);
  CU_ASSERT_EQUAL(col.grey, 20);
}

void test_scan_colour_rgb(void)
{
  colour_t col;

  CU_ASSERT_EQUAL(scan_colour("8/9/10", &col), ERROR_OK);
  CU_ASSERT_EQUAL(col.type, colour_rgb);
  CU_ASSERT_EQUAL(col.rgb.r, 8);
  CU_ASSERT_EQUAL(col.rgb.g, 9);
  CU_ASSERT_EQUAL(col.rgb.b, 10);
}

void test_scan_colour_named(void)
{
  colour_t col;

  CU_ASSERT_EQUAL(scan_colour("red", &col), ERROR_OK);
  CU_ASSERT_EQUAL(col.type, colour_rgb);
  CU_ASSERT_EQUAL(col.rgb.r, 255);
  CU_ASSERT_EQUAL(col.rgb.g, 0);
  CU_ASSERT_EQUAL(col.rgb.b, 0);
}
