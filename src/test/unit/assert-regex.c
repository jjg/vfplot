#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <CUnit/CUnit.h>
#include "assert-regex.h"

#ifdef HAVE_REGEX_H

#include <regex.h>

extern void assert_regex(const char *pattern, const char *text)
{
  regex_t regex;

  CU_ASSERT_EQUAL_FATAL(regcomp(&regex, pattern, REG_EXTENDED), 0);
  CU_ASSERT_EQUAL(regexec(&regex, text, 0, NULL, 0), 0);

  regfree(&regex);
}

#else

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
extern void assert_regex(const char *pattern, const char *text)
{
  CU_FAIL("No POSIX regex support");
}
#pragma GCC diagnostic pop

#endif
