#include <CUnit/CUnit.h>

extern CU_TestInfo tests_domain_geojson[];

void test_domain_read_nopolys(void);
void test_domain_read_square(void);
void test_domain_read_nested_squares(void);
void test_domain_read_adjacent_squares(void);
void test_domain_read_feature(void);
void test_domain_read_feature_collection(void);
void test_domain_read_geometry_collection(void);

void test_domain_write(void);
