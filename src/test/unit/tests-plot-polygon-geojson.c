#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>

#ifdef HAVE_JANSSON_H
#include <jansson.h>
#endif

#include <plot/polygon-geojson.h>

#include "assert-polygon.h"
#include "tests-plot-polygon-geojson.h"

CU_TestInfo tests_polygon_geojson[] = {
  {"GeoJSON serialise", test_polygon_geojson_serialise},
  {"GeoJSON deserialise", test_polygon_geojson_deserialise},
  CU_TEST_INFO_NULL
};


#ifdef HAVE_LIBJANSSON

#define POLYGON_JSON "[[1.0,1.0],[1.0,2.0],[2.0,2.0],[1.0,1.0]]"

void test_polygon_geojson_serialise(void)
{
  vec_t v[3] = { {1, 1}, {1, 2}, {2, 2} };
  polygon_t p = { .n = 3, .v = v };
  json_t
    *json1 = polygon_geojson_serialise(&p),
    *json2 = json_loads(POLYGON_JSON, 0, NULL);

  CU_ASSERT(json_equal(json1, json2));
  json_decref(json1);
  json_decref(json2);
}

void test_polygon_geojson_deserialise(void)
{
  vec_t v[3] = { {1, 1}, {1, 2}, {2, 2} };
  polygon_t p = { .n = 3, .v = v }, q;
  json_t *json = json_loads(POLYGON_JSON, 0, NULL);

  CU_ASSERT_EQUAL(polygon_geojson_deserialise(json, &q), 0);
  CU_ASSERT_POLYGON_EQUAL(p, q, 1e-10);

  json_decref(json);
}

#else

void test_polygon_geojson_serialise(void)
{
  json_t *json = polygon_geojson_serialise(NULL);
  CU_ASSERT_PTR_NULL(json);
}

void test_polygon_geojson_deserialise(void)
{
  polygon_t p = {0};
  json_t *json = NULL;

  CU_ASSERT_NOT_EQUAL(polygon_geojson_deserialise(json, &p), 0);
}

#endif
