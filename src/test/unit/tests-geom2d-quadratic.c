#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <geom2d/quadratic.h>

#include "tests-geom2d-quadratic.h"

CU_TestInfo tests_quadratic[] = {
  {"(x-1)(x+1)", test_quadratic_roots_complex_1},
  {"(x-2)(x+1)", test_quadratic_roots_complex_2},
  {"x+1", test_quadratic_roots_complex_3},
  {"(x+1)^2", test_quadratic_roots_complex_4},
  {"x^2+1", test_quadratic_roots_complex_5},
  {"just properly complex", test_quadratic_roots_complex_6},
  CU_TEST_INFO_NULL
};

/* (x-1)(x+1) */

void test_quadratic_roots_complex_1(void)
{
  double
    eps = 1e-10,
    c[3] = {-1, 0, 1};
  double complex s[2];

  CU_ASSERT_EQUAL(quadratic_roots_complex(c, s), 0);

  CU_ASSERT_DOUBLE_EQUAL(creal(s[0]), 1, eps);
  CU_ASSERT_DOUBLE_EQUAL(cimag(s[0]), 0, eps);

  CU_ASSERT_DOUBLE_EQUAL(creal(s[1]), -1, eps);
  CU_ASSERT_DOUBLE_EQUAL(cimag(s[1]), 0, eps);
}

/* (x-2)(x+1) */

void test_quadratic_roots_complex_2(void)
{
  double
    eps = 1e-10,
    c[3] = {-2, -1, 1};
  double complex s[2];

  CU_ASSERT_EQUAL(quadratic_roots_complex(c, s), 0);

  CU_ASSERT_DOUBLE_EQUAL(creal(s[0]), 2, eps);
  CU_ASSERT_DOUBLE_EQUAL(cimag(s[0]), 0, eps);

  CU_ASSERT_DOUBLE_EQUAL(creal(s[1]), -1, eps);
  CU_ASSERT_DOUBLE_EQUAL(cimag(s[1]), 0, eps);
}

/* x+1 */

void test_quadratic_roots_complex_3(void)
{
  double c[3] = {1, 1, 0};
  double complex s[2];

  CU_ASSERT_NOT_EQUAL(quadratic_roots_complex(c, s), 0);
}

/* (x+1)^2 */

void test_quadratic_roots_complex_4(void)
{
  double
    eps = 1e-10,
    c[3] = {1, 2, 1};
  double complex s[2];

  CU_ASSERT_EQUAL(quadratic_roots_complex(c, s), 0);

  CU_ASSERT_DOUBLE_EQUAL(creal(s[0]), -1, eps);
  CU_ASSERT_DOUBLE_EQUAL(cimag(s[0]), 0, eps);

  CU_ASSERT_DOUBLE_EQUAL(creal(s[1]), -1, eps);
  CU_ASSERT_DOUBLE_EQUAL(cimag(s[1]), 0, eps);
}

/* x^2 + 1 */

void test_quadratic_roots_complex_5(void)
{
  double
    eps = 1e-10,
    c[3] = {1, 0, 1};
  double complex s[2];

  CU_ASSERT_EQUAL(quadratic_roots_complex(c, s), 0);

  CU_ASSERT_DOUBLE_EQUAL(creal(s[0]), 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(cimag(s[0]), 1, eps);

  CU_ASSERT_DOUBLE_EQUAL(creal(s[1]), 0, eps);
  CU_ASSERT_DOUBLE_EQUAL(cimag(s[1]), -1, eps);
}

/*
  this comes from real plot, interpolation of the metric gives
  no real roots, we just assert these are properly a conjugate
  pair
*/

void test_quadratic_roots_complex_6(void)
{
  double c[3] = { 2.3256803936137784e-09, -9.6450617283950612e-05, 1 };
  double complex s[2];

  /*
    the quadratic is minimised when c[1] + 2 c[2] x = 0,
    so x = -c[1] / 2 c[2]
  */

  double x0 = -c[1] / (2 * c[2]);

  /* evaluate the polynomial at x0 (Horner) */

  double y0 = c[0] + x0 * ( c[1] + x0 * c[2] );

  CU_ASSERT(y0 > 0);
  CU_ASSERT_EQUAL(quadratic_roots_complex(c, s), 0);
  CU_ASSERT_EQUAL(creal(s[0]), creal(s[1]));
  CU_ASSERT_EQUAL(cimag(s[0]), -cimag(s[1]));
}
