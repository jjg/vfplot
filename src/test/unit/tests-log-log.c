#include <log.h>

#include "tests-log-log.h"
#include "assert-regex.h"

#include <stdlib.h>
#include <unistd.h>

CU_TestInfo tests_log[] = {
  {"plain", test_log_plain},
  {"decorated", test_log_decorated},
  {"level from string", test_log_level_of_string},
  CU_TEST_INFO_NULL,
};

/*
  Check that under log level WARN, error and warn messages
  are written to file, but info messages are not; the only
  difference is the regexp of the expected output
*/

extern void test_log_plain(void)
{
  char path[] = "/tmp/test-log-plain-XXXXXX";

  int fd = mkstemp(path);
  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);

  FILE *fp = fdopen(fd, "w");
  CU_ASSERT_PTR_NOT_NULL_FATAL(fp);

  CU_ASSERT_EQUAL(log_add_plain(fp, LOG_WARN), 0);
  log_set_level(LOG_INFO);

  log_info("info");
  log_warn("warn");
  log_error("error");

  fclose(fp);

  fp = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(fp);

  char line[256];

  CU_ASSERT_PTR_NOT_NULL(fgets(line, 256, fp));
  CU_ASSERT_REGEX("^warn", line);

  CU_ASSERT_PTR_NOT_NULL(fgets(line, 256, fp));
  CU_ASSERT_REGEX("^error", line);

  CU_ASSERT_PTR_NULL(fgets(line, 256, fp));

  fclose(fp);
  unlink(path);
  log_reset();
}

extern void test_log_decorated(void)
{
  char path[] = "/tmp/test-log-decorated-XXXXXX";

  int fd = mkstemp(path);
  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);

  FILE *fp = fdopen(fd, "w");
  CU_ASSERT_PTR_NOT_NULL_FATAL(fp);

  CU_ASSERT_EQUAL(log_add_decorated(fp, LOG_WARN), 0);
  log_set_level(LOG_INFO);

  log_info("info");
  log_warn("warn");
  log_error("error");

  fclose(fp);

  fp = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(fp);

  char line[256];

  CU_ASSERT_PTR_NOT_NULL(fgets(line, 256, fp));
  CU_ASSERT_REGEX("^.* WARN .*: warn", line);

  CU_ASSERT_PTR_NOT_NULL(fgets(line, 256, fp));
  CU_ASSERT_REGEX("^.* ERROR .*: error", line);

  CU_ASSERT_PTR_NULL(fgets(line, 256, fp));

  fclose(fp);
  unlink(path);
  log_reset();
}

static void check_level(int expected, const char *string)
{
  int obtained = log_level_of_string(string);
  CU_ASSERT_EQUAL(expected, obtained);
}

extern void test_log_level_of_string(void)
{
  check_level(LOG_TRACE, "trace");
  check_level(LOG_TRACE, "TRACE");
  check_level(LOG_DEBUG, "debug");
  check_level(LOG_DEBUG, "DEBUG");
  check_level(LOG_INFO, "info");
  check_level(LOG_INFO, "INFO");
  check_level(LOG_WARN, "warn");
  check_level(LOG_WARN, "WARN");
  check_level(LOG_ERROR, "error");
  check_level(LOG_ERROR, "ERROR");
  check_level(LOG_FATAL, "fatal");
  check_level(LOG_FATAL, "FATAL");
  check_level(-1, "quack");
}
