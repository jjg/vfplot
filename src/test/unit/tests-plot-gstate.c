#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>

#include <plot/gstate.h>

#include "tests-plot-gstate.h"
#include "fixture.h"

#ifdef WITH_JSON

CU_TestInfo tests_gstate[] = {
  {"write, empty", test_gstate_write_empty},
  {"read, minimal", test_gstate_read_minimal},
  {"read, not object", test_gstate_read_not_object},
  {"read, no arrows", test_gstate_read_no_arrows},
  {"read, arrows not array", test_gstate_read_arrows_not_array},
  {"read, arrow not object", test_gstate_read_arrow_not_object},
  {"read, arrow minimal", test_gstate_read_arrow_minimal},
  {"read, arrow no theta", test_gstate_read_arrow_no_theta},
  {"read, arrow theta not real", test_gstate_read_arrow_theta_not_real},
  {"read, arrow no length", test_gstate_read_arrow_no_length},
  {"read, arrow length not real", test_gstate_read_arrow_length_not_real},
  {"read, arrow no width", test_gstate_read_arrow_no_width},
  {"read, arrow width not real", test_gstate_read_arrow_width_not_real},
  {"read, arrow no curve", test_gstate_read_arrow_no_curve},
  {"read, arrow curve not real", test_gstate_read_arrow_curve_not_real},
  {"read, arrow no centre", test_gstate_read_arrow_no_centre},
  {"read, arrow centre not object", test_gstate_read_arrow_centre_not_object},
  {"read, arrow no x", test_gstate_read_arrow_no_x},
  {"read, arrow x not real", test_gstate_read_arrow_x_not_real},
  {"read, arrow no y", test_gstate_read_arrow_no_y},
  {"read, arrow y not real", test_gstate_read_arrow_y_not_real},
  {"read, no nbrs", test_gstate_read_no_nbrs},
  {"read, nbrs not array", test_gstate_read_nbrs_not_array},
  {"read, nbr not array", test_gstate_read_nbr_not_array},
  {"read, nbr minimal", test_gstate_read_nbr_minimal},
  {"read, nbr not pair", test_gstate_read_nbr_not_pair},
  {"read, nbr 1st not object", test_gstate_read_nbr_1st_not_object},
  {"read, nbr 2nd not object", test_gstate_read_nbr_2nd_not_object},
  {"read, nbr no id", test_gstate_read_nbr_no_id},
  {"read, nbr id not real", test_gstate_read_nbr_id_not_real},
  {"read, nbr no centre", test_gstate_read_nbr_no_centre},
  {"read, nbr centre not object", test_gstate_read_nbr_centre_not_object},
  {"read, nbr no x", test_gstate_read_nbr_no_x},
  {"read, nbr x not real", test_gstate_read_nbr_x_not_real},
  {"read, nbr no y", test_gstate_read_nbr_no_y},
  {"read, nbr y not real", test_gstate_read_nbr_y_not_real},
  CU_TEST_INFO_NULL,
};

void test_gstate_write_empty(void)
{
  gstate_t gstate = {
    .arrows = {
      .n = 0,
      .v = NULL
    },
    .nbrs = {
      .n = 0,
      .v = NULL
    }
  };

  char filename[] = "/tmp/gstate-write-empty-XXXXXX";
  int fd = mkstemp(filename);
  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);

  FILE *stream = fdopen(fd, "w");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  CU_ASSERT_EQUAL(gstate_write(stream, &gstate), 0);
  fclose(stream);

  CU_ASSERT_EQUAL(access(filename, F_OK), 0);
  CU_ASSERT_EQUAL(unlink(filename), 0);
}

void test_gstate_read_minimal(void)
{
  const char *path = fixture("gstate/minimal.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_not_object(void)
{
  const char *path = fixture("gstate/not-object.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_no_arrows(void)
{
  const char *path = fixture("gstate/no-arrows.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrows_not_array(void)
{
  const char *path = fixture("gstate/arrows-not-array.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_not_object(void)
{
  const char *path = fixture("gstate/arrow-not-object.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_minimal(void)
{
  const char *path = fixture("gstate/arrow-minimal.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_no_theta(void)
{
  const char *path = fixture("gstate/arrow-no-theta.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_theta_not_real(void)
{
  const char *path = fixture("gstate/arrow-theta-not-real.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_no_length(void)
{
  const char *path = fixture("gstate/arrow-no-length.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_length_not_real(void)
{
  const char *path = fixture("gstate/arrow-length-not-real.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_no_width(void)
{
  const char *path = fixture("gstate/arrow-no-width.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_width_not_real(void)
{
  const char *path = fixture("gstate/arrow-width-not-real.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_no_curve(void)
{
  const char *path = fixture("gstate/arrow-no-curve.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_curve_not_real(void)
{
  const char *path = fixture("gstate/arrow-curve-not-real.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_no_centre(void)
{
  const char *path = fixture("gstate/arrow-no-centre.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_centre_not_object(void)
{
  const char *path = fixture("gstate/arrow-centre-not-object.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_no_x(void)
{
  const char *path = fixture("gstate/arrow-no-x.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_x_not_real(void)
{
  const char *path = fixture("gstate/arrow-x-not-real.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_no_y(void)
{
  const char *path = fixture("gstate/arrow-no-y.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_arrow_y_not_real(void)
{
  const char *path = fixture("gstate/arrow-y-not-real.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_no_nbrs(void)
{
  const char *path = fixture("gstate/no-nbrs.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbrs_not_array(void)
{
  const char *path = fixture("gstate/nbrs-not-array.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_not_array(void)
{
  const char *path = fixture("gstate/nbr-not-array.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_minimal(void)
{
  const char *path = fixture("gstate/nbr-minimal.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_not_pair(void)
{
  const char *path = fixture("gstate/nbr-not-pair.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_1st_not_object(void)
{
  const char *path = fixture("gstate/nbr-1st-not-object.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_2nd_not_object(void)
{
  const char *path = fixture("gstate/nbr-2nd-not-object.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_no_id(void)
{
  const char *path = fixture("gstate/nbr-no-id.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_id_not_real(void)
{
  const char *path = fixture("gstate/nbr-id-not-real.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_no_centre(void)
{
  const char *path = fixture("gstate/nbr-no-centre.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_centre_not_object(void)
{
  const char *path = fixture("gstate/nbr-centre-not-object.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_no_x(void)
{
  const char *path = fixture("gstate/nbr-no-x.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_x_not_real(void)
{
  const char *path = fixture("gstate/nbr-x-not-real.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_no_y(void)
{
  const char *path = fixture("gstate/nbr-no-y.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

void test_gstate_read_nbr_y_not_real(void)
{
  const char *path = fixture("gstate/nbr-y-not-real.json");
  FILE *stream = fopen(path, "r");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);
  gstate_t gstate;

  CU_ASSERT_NOT_EQUAL(gstate_read(stream, &gstate), 0);
  fclose(stream);
}

#else

CU_TestInfo tests_gstate[] = { CU_TEST_INFO_NULL };

#endif
