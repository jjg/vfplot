# vfplot Makefile

RUBBISH = *~
CONFFILES = config.cache config.log config.status

# user targets

default: all

all:
	$(MAKE) -C src all

install:
	$(MAKE) -C src install

clean:
	$(MAKE) -C src clean
	$(RM) $(RUBBISH)

spotless veryclean:
	$(MAKE) -C src spotless
	$(RM) $(RUBBISH) $(CONFFILES)
	rm -rf autom4te.cache/

.PHONY: default all install clean spotless veryclean clean

# test

TESTS := test-unit test-accept test-memory test-visual test-case

test: $(TESTS)

test-unit:
	$(MAKE) -C src test-unit

test-accept:
	$(MAKE) -C src test-accept

test-memory:
	$(MAKE) -C src test-memory

test-visual:
	$(MAKE) -C src test-visual

test-visual-show:
	$(MAKE) -C src test-visual-show

test-case:
	$(MAKE) -C src test-case

.PHONY: test $(TESTS)

# maintainer targets

# profile

profile-prepare:
	./configure --enable-profile
	$(MAKE) all

profile-run:
	$(MAKE) -C src profile

profile-clean: spotless

update-profile: profile-prepare profile-run profile-clean

.PHONY: update-profile profile-prepare profile-run profile-clean

# coverage

coverage-prepare:
	./configure \
		--enable-coverage \
		--enable-unit-tests \
		--enable-acceptance-tests \
		--enable-json \
		--enable-gerris \
		--enable-matlab \
		--enable-netcdf
	$(MAKE) all test

coverage-run:
	$(MAKE) -C src coverage

coverage-show:
	$(MAKE) -C src coverage-show

coverage-clean: spotless

.PHONY: coverage-prepare coverage-run coverage-show coverage-clean

update-coverage: coverage-prepare coverage-run coverage-clean

.PHONY: update-coverage

# coverity tests

coverity:
	bin/coverity-build
	bin/coverity-upload
	bin/coverity-clean

.PHONY: coverity

# scan-build, this is a full configure-make-spotless cycle

scan-build:
	scan-build -v ./configure
	scan-build -v -maxloop 32 $(MAKE) -s
	$(MAKE) -s spotless

.PHONY: scan-build

# root documentation, needs pandoc installed

root-docs:
	$(MAKE) -C src root-docs

.PHONY: root-docs

# librtree, this is copied-in from a different repository

LIBRTREE_BRANCH = master

update-librtree-self:
	bin/librtree-update \
		--branch $(LIBRTREE_BRANCH) \
		--verbose \
		--self

update-librtree:
	bin/librtree-update \
		--branch $(LIBRTREE_BRANCH) \
		--verbose \
		--target src/librtree

update-librtree-clean:
	bin/librtree-update \
		--branch $(LIBRTREE_BRANCH) \
		--verbose \
		--clean

update-librtree-depend:
	$(MAKE) -C src/librtree update-depend

.PHONY: update-librtree-self update-librtree update-librtree-depend

# this needs to be done once on first cloning the repo, only if you
# plan to develop the library: it symlinks files in .hooks (which
# is version controlled) to .git/hooks (which is not).

git-setup-hooks:
	bin/git-setup-hooks

.PHONY: git-setup-hooks

# note that ./configure creates the version file VERSION, the variable
# $(VERSION) reads that file, so one should not use that variable
# until ./configure has been invoked, it will contain the old version.

VERSION = $(shell cat VERSION)

# git targets used by release

git-release-commit:
	git add -u
	git commit -m $(VERSION)
	git push origin master

git-create-tag:
	bin/git-release-tag $(VERSION)

git-push-tag:
	git push origin v$(VERSION)

.PHONY: git-release-commit git-create-tag git-push-tag

# release targets proper

update-autoconf:
	aclocal --output config/aclocal.m4
	autoconf
	autoheader

update-assets:
	./configure --enable-gerris
	bin/version-assets
	$(MAKE)
	$(MAKE) spotless

update: update-autoconf update-assets

release: update git-release-commit git-create-tag git-push-tag

.PHONY: update-autoconf update-assets update release

# distribution

DIST = vfplot-$(VERSION)

dist:
	tar -C .. \
	  --transform s/^vfplot/$(DIST)/ \
	  --exclude-from .distignore \
	  -zpcvf ../$(DIST).tar.gz vfplot

.PHONY: dist
